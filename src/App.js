import {React, Switch } from 'react'
import './App.css';
import Navigation  from './components/shared/navigation'
import Footer from './components/shared/footer'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
// import Patients from './components/pages/patient'
import Patients from './components/pages/patients'
import PatientView from './components/pages/patientView'
import AllergiesAdd from './components/pages/allergiesAdd'
// import PatientContactDetailsEdit from './components/pages/patientContactDetailsEdit'
// import PatientContactDetailsReadOnly from './components/pages/patientContactDetailsReadOnly'
// import PatientDemographicsEdit from './components/pages/patientDemographicsEdit'
// import PatientDemographicsReadOnly from './components/pages/patientDemographicsReadOnly'


import MedHistory from './components/pages/medHistory'
import MedHistoryAdd from './components/pages/medHistoryAdd';
import MedHistoryEdit from './components/pages/medHistoryEdit'
import Meds from './components/pages/meds'
import Allergies from './components/pages/allergies'
import Immune from './components/pages/immune'
import Lab from './components/pages/lab'
import PatientLabEdit from './components/pages/patientLabEdit'
import PatientLabAdd from './components/pages/patientLabAdd'
import Radio from './components/pages/radio'
import Billing from './components/pages/billing'
import BillingAdd from './components/pages/billingAdd'
import BillingEdit from './components/pages/billingEdit'
import Notes from './components/pages/notes'
import PatientNotesAdd from './components/pages/patientNotesAdd'
import PatientNotesEdit from './components/pages/patientNotesEdit'
import Admin from './components/pages/admin'
import EditPatient from './components/pages/EditPatient';
import Login from './components/pages/login';
import Careadmin from './components/pages/careadmin';
import EditCareprovider from './components/pages/EditCareprovider';
import RevisionHistory from './components/pages/revisionHistory'
import MedsAdd from './components/pages/medsAdd';
import ImmuneAdd from './components/pages/immuneAdd'
import RadioAdd from './components/pages/radioAdd'
import RadioEdit from './components/pages/radioEdit'
// import PatientEmergencyReadOnly from './components/pages/patientEmergencyReadOnly';
// import PatientEmergencyEdit from './components/pages/patientEmergencyEdit';



// import PrivateRoute from './components/shared/PrivateRoute'

function App() {
  return (
    <BrowserRouter>  
        <Routes>
        <Route exact path="/EditCarepeovider" element={<EditCareprovider />} />
        <Route exact path="/EditPatient/:id" element={<EditPatient />} />
        <Route exact path="/careadmin" element={<Careadmin />} />
        <Route exact path="/" element={<Login />} />
          <Route exact path="/patients" element={<Patients />} />
          <Route exact path="/patients/:id" element={<PatientView />} />
          {/* <Route exact path="/patients/:id/emergency/edit/contactID" element={<PatientEmergencyEdit />} /> */}
          <Route exact path="/patients/:id/medHistory" element={<MedHistory />} />
          <Route exact path="/patients/:id/medHistory/add" element={<MedHistoryAdd />} />
          <Route exact path="/patients/:id/medHistory/edit" element={<MedHistoryEdit />} />
          <Route exact path="/patients/:id/meds" element={<Meds />} />
          <Route exact path="/patients/:id/meds/add" element={<MedsAdd />} />   
          <Route exact path="/patients/:id/allergies" element={<Allergies />} />
          <Route exact path="/patients/:id/allergies/add" element={<AllergiesAdd />} />
          <Route exact path="/patients/:id/immune" element={<Immune />} />
          <Route exact path="/patients/:id/immune/add" element={<ImmuneAdd />} />
          <Route exact path="/patients/:id/lab" element={<Lab />} />
          <Route exact path="/patients/:id/lab/add" element={<PatientLabAdd />} />
          <Route exact path="/patients/:id/lab/edit/:labID" element={<PatientLabEdit />} />
          <Route exact path="/patients/:id/radio" element={<Radio />} />
          <Route exact path="/patients/:id/radiology/add" element={<RadioAdd />} />
          <Route exact path="/patients/:id/radiology/edit" element={<RadioEdit />} />
          <Route exact path="/patients/:id/billing" element={<Billing />} />
          <Route exact path="/patients/:id/billing/add" element={<BillingAdd />} />
          <Route exact path="/patients/:id/billing/edit" element={<BillingEdit />} />
          <Route exact path="/patients/:id/notes" element={<Notes />} />
          <Route exact path="/patients/:id/notes/add" element={<PatientNotesAdd />} />
          <Route exact path="/patients/:id/notes/edit/:noteID" element={<PatientNotesEdit />} />
          <Route exact path="/admin" element={<Admin />} />  
          <Route exact path="/patients/:id/revision_history" element={<RevisionHistory />} />
         {/*} <PrivateRoute path="/submissions">
  </PrivateRoute> */}

        </Routes>
        <Footer />  
    </BrowserRouter>
  );
}

export default App;
