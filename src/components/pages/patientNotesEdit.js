import React, { useState, useEffect } from "react";
import { useParams } from "react-router";
import { Container, Form, FormGroup, Label, Input, Row, Col, Table, Button, CardBody, CardText, Card } from 'reactstrap'
import Navigation  from '../shared/navigation'
import { useNavigate } from "react-router-dom";


const PatientNotesEdit = () => {
    const { id, noteID } = useParams();
    const navigate = useNavigate();

    const patientId = id;
    const patientNoteID = noteID;

    const [chart_entry_date, setChart_entry_date] = useState("")
    const [comments, setComments] = useState("")
    const [patient_id, setPatient_id] = useState("")
    const [care_provider_id, setCare_provider_id] = useState("")

    const [notesEditFormData, setNotesEditFormData] = useState({
        chart_entry_date: '', 
        comments : '', 
        patient_id: '', 
        care_provider_id: ''
    })



    // Function to format date
    const formatDateToYYYMMDDHHMMSS = (dateFieldToFormat) => {
        const formattedDate = dateFieldToFormat.getFullYear() + '-' + (dateFieldToFormat.getMonth()+1) + '-' + dateFieldToFormat.getDate() +' '+ dateFieldToFormat.getHours()+':'+ dateFieldToFormat.getMinutes()+':'+ dateFieldToFormat.getSeconds();
        return formattedDate;
    }


    useEffect(() => {
        const fetchData = async () => {
          const notesRes = await fetch(
            `http://localhost:4000/api/patient_notes/${patientId}/${patientNoteID}`
          );
          const res = await notesRes.json();
             setNotesEditFormData(res[0]);
        };
        fetchData();
      }, [patientId, patientNoteID]);
    

   // Handles inputs in NOTES EDIT FORM 
   const handleEditNotesForm_FieldValueChange = (event) => {
        event.preventDefault();
        const notesFieldName = event.target.getAttribute("name");
        const notesFieldValue = event.target.value;
        const newNotesFormData = { ...notesEditFormData };
        newNotesFormData[notesFieldName] = notesFieldValue
        setNotesEditFormData(newNotesFormData)
    }   


       // Used to handle the SAVE button in the EDIT form
       // NOTE: mySQL DateTIME data type stores data as '2021-12-01 00:00:00' date format
       // JS retrieves mySQL DATETIME datatype as string date in this format '2021-12-02T06:49:19.000Z'
       const handleEditNotesForm_SubmitButton = (event) => {
            event.preventDefault()

            // Convert chart_entry date from string to date
            // Return value: Date in this format 'Thu Dec 02 2021 10:00:56 GMT-0500 (Eastern Standard Time)'
            // const stringDateToDate = new Date(notesEditFormData.chart_entry_date)


            // Format the date as YYYY=MM-DD HH-MM-SS
            // JS returns date in this format '2021-12-02T15:00:56.000Z'
            // const newChartEntryDate = formatDateToYYYMMDDHHMMSS(stringDateToDate)

        

            const editedNotesData = {
                chart_entry_date: notesEditFormData.chart_entry_date,
                comments: notesEditFormData.comments,
                patient_id: notesEditFormData.patient_id,
                care_provider_id: notesEditFormData.care_provider_id
            }
    
            fetch(`http://localhost:4000/api/patient_notes/${patientNoteID}`, {
            method: "put",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },    
                body: JSON.stringify(notesEditFormData),
              }).then((response) => response.json());   
                    navigate(`/patients/${patientId}/notes`);          
        };

    

         const redirectToNotesList = (event) => {
             event.preventDefault();
                navigate(`/patients/${id}/notes`);
          };



    return(
        <Container style={{ backgroundColor: "#ADD8E6" }} className="my-5">
         <Navigation patientID={id} />  
            <Row className="my-5">
                 <div className="editContactDetails-maincontainer">
                    <Form onSubmit = {handleEditNotesForm_SubmitButton}>
                        <Row>
                            <div className="editContactDetails-cardTitle-container">
                            <Col lg="6" sm="6" xs="12">
                                <div className="editContactDetails-header-container">
                                    Edit Notes Entry
                                </div>    
                            </Col>
                            <Col lg="6" sm="6" xs="12">              
                                <div className="editContactDetails-button-container">
                                    <Button size="sm" className="editContactDetails-save-button"
                                    >Submit
                                    </Button>

                                    <Button size="sm" className="editContactDetails-submit-button"
                                        onClick={redirectToNotesList}
                                        >Cancel
                                    </Button>                                                                           
                                </div>
                            </Col>
                            </div>
                            <br></br> <br></br>
                        </Row>

                        <Row>
                            <div className="editContactDetails-container">
                                <Col lg="6" sm="6" xs="12">
                                    <div id="editContactDetails-mainInfo-container"> 

                                        <label id="editContactDetails-label" className="editContactDetails-label">Comments   :</label> 
                                        <textarea id="editContactDetails-textbox" className="editContactDetails-textbox"
                                            type="text"
                                            name="comments"
                                            required="required"
                                            value={notesEditFormData.comments}
                                            onChange={handleEditNotesForm_FieldValueChange}
                                        ></textarea> <br></br>

                                        <label id="editContactDetails-label" className="editContactDetails-label">Care Provider  :   </label> 
                                        <input id="editContactDetails-textbox" className="editContactDetails-textbox" 
                                            type="text"
                                            name="care_provider_id"
                                            required="required"
                                            onChange={handleEditNotesForm_FieldValueChange}                             
                                            value={notesEditFormData.care_provider_id}
                                        ></input> <br></br>
                                    </div>
                                </Col>
                            </div>  
                        </Row>    
                    </Form> 
                      
                </div>
            </Row>
            <Card className="text-white bg-secondary my-5 py-4 text-center">
                <CardBody>
                    <CardText>
                        <span className="text-white m-0"></span>
                    </CardText>
                </CardBody>
            </Card>
            
        </Container>
    )
}

export default PatientNotesEdit