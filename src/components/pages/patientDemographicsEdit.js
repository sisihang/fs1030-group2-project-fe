import React, { useState, useEffect } from "react";
import { useParams } from "react-router";
import { NavLink as RouteLink } from "react-router-dom";
import { Container, Form, FormGroup, Label, Input, Row, Col, Table, Button } from 'reactstrap'
// import Select from 'react-select';
import "../styles/patientDemographicsEdit.css"
// import ProvinceLookup from "./lookup/ProvinceLookup-toremove";



const PatientDemographicsEdit = ({patient, demoEditFormData, handleDemoEditFormChange, handleDemoEditClick, handleDemoEditCancelClick}) => {

  const [genderLookupValue, setGenderLookupValue] = useState([])
  const [selectedGender, setSelectedGender] = useState({
        gender_code: patient.gender_code,
        gender_description: patient.gender_description
  })

  const [raceLookupValue, setRaceLookupValue] = useState([])
  const [selectedRace, setSelectedrace] = useState({
        race_id: patient.race_id,
        race_name: patient.race_name
  })


  const [maritalStatusLookupValue, setMaritalStatusLookupValue] = useState([])
  const [selectedMaritalStatus, setSelectedMaritalStatus] = useState({
        marital_status_code: patient.marital_status_code,
        marital_status_desc: patient.marital_status_desc
  })  


  const [prefLanguageLookupValue, setPrefLanguageLookupValue] = useState([])
  const [selectedPrefLanguage, setSelectedPrefLanguage] = useState({
        pref_language_id: patient.pref_language_id,
        pref_language_name: patient.pref_language_name
  })



 

  useEffect(() => {
    async function fetchData() {
      const res = await fetch("http://localhost:4000/api/gender");
      res
        .json()
        .then((res) => setGenderLookupValue(res))
        .catch((err) => console.log(err));
    }
      fetchData();
  }, []);

  useEffect(() => {
    async function fetchData() {
      const res = await fetch("http://localhost:4000/api/race");
      res
        .json()
        .then((res) => setRaceLookupValue(res))
        .catch((err) => console.log(err));
    }
      fetchData();
  }, []);


  useEffect(() => {
    async function fetchData() {
      const res = await fetch("http://localhost:4000/api/maritalstatus");
      res
        .json()
        .then((res) => setMaritalStatusLookupValue(res))
        .catch((err) => console.log(err));
    }
      fetchData();
  }, []);


  useEffect(() => {
    async function fetchData() {
      const res = await fetch("http://localhost:4000/api/preferredlanguage");
      res
        .json()
        .then((res) => setPrefLanguageLookupValue(res))
        .catch((err) => console.log(err));
    }
      fetchData();
  }, []);


      // Function to format date
      const formatDateToYYYMMDD = (dateFieldToFormat) => {
        // patient.dob in mySQL is DATETIME and stored in the table in this format '1975-07-12 11:20:20'
        // JS pulls the date as string in this fromat '1975-07-12 11:20:20'

        // This returns in this format '7/12/1975' as a string type
        // const formattedDate = new Date(dateFieldToFormat).toLocaleDateString();

        // alert(formattedDate)
        // alert(typeof(formattedDate))



       

        // return formattedDate;
    } 


  const handleGenderChange = (e) => {
    setSelectedGender({gender_code: e.target.value})
  }

  const handleRaceChange = (e) => {
    setSelectedrace({gender_code: e.target.value})
  }

  const handleMaritalStatusChange = (e) => {
    setSelectedMaritalStatus({gender_code: e.target.value})
  }

  const handlePrefLanguageChange = (e) => {
    setSelectedPrefLanguage({gender_code: e.target.value})
  }
  

    return (
        <div className="editDemo-maincontainer">
            <Row>
                <div className="editDemo-cardTitle-container">
                  <Col lg="6" sm="6" xs="12">
                    <div className="editDemo-header-container">
                         Demographics
                    </div>    
                  </Col>
                  <Col lg="6" sm="6" xs="12">              
                    <div className="editDemo-button-container">
                          <Button size="sm" className="editDemo-save-button"
                            // onClick={(e) => handleEdit_ContactDetails(e, patient)}

                          >Save
                          </Button>

                          <Button size="sm" className="editDemo-cancel-button"
                              onClick={handleDemoEditCancelClick}
                            >Cancel
                         </Button>                                                                           
                    </div>
                  </Col>
                  </div>
                  <br></br> <br></br>
              </Row>

              <Row>
                <div className="editDemo-container">
                    <Col lg="6" sm="6" xs="12">
                        <div id="editDemo-mainInfo-container"> 

                            <label id="editDemo-label" className="editDemo-label">DOB   :</label> 
                            <input id="editDemo-dobTextbox" className="editDemo-textbox"
                                type="date"
                                // required="required"
                                name="dob"
                                value={demoEditFormData.dob}
                                // defaultValue={demoEditFormData.dob}
                                // defaultValue={formatDateToYYYMMDD(demoEditFormData.dob)}
                                onChange={demoEditFormData.dob}
                            ></input> <br></br>

                            {/* <label id="editDemo-label" className="editDemo-label">Age  :   </label> 
                            <input id="editDemo-textbox" className="editDemo-textbox" 
                                type="text"
                                // required="required"
                                name="patient_mname"
                                value={contactDetailsEditFormData.patient_mname}                                
                                 onChange={handleContactDetailsEditFormChange}
                            ></input> <br></br> */}
                            
                            {/* <label id="editDemo-label" className="editDemo-label">Gender  :</label> 
                            <select
                                id="editDemo-textbox"
                                className="editDemo-textbox"
                                name="gender_code"
                                value={demoEditFormData.gender_code}
                                defaultValue={demoEditFormData.gender_description}
                                onChange={handleDemoEditFormChange}
                              >
                                <option value="F">Female</option>
                                <option value="M">Male</option>
                                <option value="O">Other</option>
                              </select> <br></br> */}

                            <label id="editDemo-label" className="editDemo-label">Gender  :</label> 
                            <select                           
                                    id="editDemo-textbox" 
                                    className="editDemo-textbox"
                                    onChange = {handleGenderChange}
                             >
                                 {genderLookupValue.map( (gender) => (  
                                     <option key={gender.index} 
                                             value={gender.gender_code}
                                             selected={(selectedGender.gender_code === gender.gender_code) ? 'selected' : ''}
                                     >{gender.gender_description}</option>
                                 ))}
                            </select> <br></br>                                 

                              {/* <label id="editDemo-label" className="editDemo-label">Marital Status  :</label> 
                              <select
                                id="editDemo-textbox"
                                className="editDemo-textbox"
                                name="marital_status_code"
                                value={demoEditFormData.marital_status_code}
                                defaultValue={demoEditFormData.marital_status_code}
                                onChange={handleDemoEditFormChange}
                              >
                                <option value="S">Single</option>
                                <option value="M">Married</option>
                                <option value="D">Divorced</option>
                                <option value="W">Widowed</option>
                                <option value="O">Other</option>
                              </select> <br></br> <br></br>           */}
                              
                              <label id="editDemo-label" className="editDemo-label">Marital Status  :</label> 
                              <select                           
                                    id="editDemo-textbox" 
                                    className="editDemo-textbox"
                                    onChange = {handleMaritalStatusChange}
                             >
                                 {maritalStatusLookupValue.map( (maritalStatus) => (  
                                     <option key={maritalStatus.index} 
                                             value={maritalStatus.marital_status_code}
                                             selected={(selectedMaritalStatus.marital_status_code === maritalStatus.marital_status_code) ? 'selected' : ''}
                                     >{maritalStatus.marital_status_desc}</option>
                                 ))}
                            </select> <br></br> 

                              {/* <label id="editDemo-label" className="editDemo-label">Race  :</label> 
                              <select
                                id="editDemo-textbox"
                                className="editDemo-textbox"
                                name="race_id"
                                value={demoEditFormData.race_id}
                                defaultValue={demoEditFormData.race_id}
                                onChange={handleDemoEditFormChange}
                              >
                                <option value="1">Asian</option>
                                <option value="2">French</option>
                                <option value="3">Scottish</option>
                                <option value="4">English</option>
                                <option value="5">Canadian</option>
                                <option value="6">East Indian</option>
                                <option value="7">North American Indian</option>
                                <option value="8">Italian</option>
                                <option value="9">Chinese</option>
                                <option value="10">German</option>
                                <option value="11">Irish</option>
                                <option value="12">Other</option>
                              </select> <br></br>   */}

                              <label id="editDemo-label" className="editDemo-label">Race  :</label>                                      
                              <select                           
                                      id="editDemo-textbox" 
                                      className="editDemo-textbox"
                                      onChange = {handleRaceChange}
                              >
                                  {raceLookupValue.map( (race) => (  
                                      <option key={race.index} 
                                              value={race.race_id}
                                              selected={(selectedRace.race_id === race.race_id) ? 'selected' : ''}
                                      >{race.race_name}</option>
                                  ))}
                              </select> <br></br>  

                              {/* <label id="editDemo-label" className="editDemo-label">Preferred Language  :</label>  */}
                              {/* <select
                                id="editDemo-textbox"
                                className="editDemo-textbox"
                                name="pref_language_id"
                                value={demoEditFormData.pref_language_id}
                                defaultValue={demoEditFormData.pref_language_id}
                                onChange={handleDemoEditFormChange}
                              >
                                <option value="1">English</option>
                                <option value="2">French</option>
                                <option value="3">Other</option>
                              </select> <br></br>  <br></br>     */}

                              <label id="editDemo-label" className="editDemo-label">Preferred Language  :</label>                               
                              <select                           
                                      id="editDemo-textbox" 
                                      className="editDemo-textbox"
                                      onChange = {handlePrefLanguageChange}
                              >
                                  {prefLanguageLookupValue.map( (prefLanguage) => (  
                                      <option key={prefLanguage.index} 
                                              value={prefLanguage.pref_language_id}
                                              selected={(selectedPrefLanguage.pref_language_id === prefLanguage.pref_language_id) ? 'selected' : ''}
                                      >{prefLanguage.pref_language_name}</option>
                                  ))}
                              </select> <br></br>                          

                        </div>
                    </Col>

                    <Col lg="6" sm="6" xs="12">
                        <div id="editDemo-workInfo-container">

                            <label id="editDemo-label" className="editDemo-label">Employer Name   :</label> 
                            <input id="editDemo-textbox" className="editDemo-textbox"
                                type="text"
                                name="employer_name"
                                value={demoEditFormData.employer_name}
                                defaultValue={demoEditFormData.employer_name}
                                onChange={handleDemoEditFormChange}
                            ></input> <br></br>     

                            <label id="editDemo-label" className="editDemo-label">Work Phone   :</label> 
                            <input id="editDemo-textbox" className="editDemo-textbox"
                                type="text"
                                name="work_phone"
                                value={demoEditFormData.work_phone}
                                defaultValue={demoEditFormData.work_phone}
                                onChange={handleDemoEditFormChange}
                            ></input> <br></br>   

                            <label id="editDemo-label" className="editDemo-label">Occupation   :</label> 
                            <input id="editDemo-textbox" className="editDemo-textbox"
                                type="text"
                                name="occupation"
                                value={demoEditFormData.occupation}
                                defaultValue={demoEditFormData.occupation}
                                onChange={handleDemoEditFormChange}
                            ></input> <br></br>                                                                                                                                             
                
                        </div>
                    </Col>                       

              
                </div>  
                 
              </Row> 
        </div>
    )
}

export default PatientDemographicsEdit
