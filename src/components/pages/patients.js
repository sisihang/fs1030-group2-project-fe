import React, { useEffect, useState } from 'react'
import { Container, Form, FormGroup, Label, Input, Row, Col, Table, Button } from 'reactstrap'
// import { useHistory } from "react-router-dom";
import { useNavigate } from 'react-router-dom';
import "../styles/patientsList.css"


const Patients = () => {

    // const history = useHistory();
    const navigate = useNavigate();

    // const token = sessionStorage.getItem('token')

    const [patientList, setPatientList] = useState([])
    const [searchOhip, setSearchOhip] = useState([])
    // const [errorMessages, setErrorMessages] = useState("")


    const handleHealthCardNoChange = event => {
        setSearchOhip(event.target.value);
    };

    useEffect(() => {
        async function fetchData() {
          const res = await fetch("http://localhost:4000/api/patients");
          res
            .json()
            .then((res) => setPatientList(res))
            .catch((err) => console.log(err));
        }
        fetchData();
    }, []);

    
    const patientViewRoute = (event, patient) => {
        event.preventDefault();
        const path = `/patients/${patient.patient_id}`;
        // history.push(path);
       navigate(path);
    }; 

    return (
        <Container>
            <div className="patientList-main-container">
                <section className="patient-main-container">
                    <Row>
                        <Form className="my-4">
                            <FormGroup row>
                                <Col xs={12} sm={4} md={3} lg={3}>     
                                    <Label style={{fontSize: ".8rem"}}>
                                        Health Card Number</Label>
                                </Col>
                                <Col xs={12} sm={6} md={5} lg={4}>  
                                        <Input 
                                            type="text" 
                                            name="search-healthCardNo" 
                                            id="search-healthCardNo" 
                                            placeholder="Enter Health Card Number" 
                                            style={{fontSize: ".8rem"}} 
                                            maxlength="10"
                                            required
                                            value={searchOhip}
                                            onChange={handleHealthCardNoChange}
                                        />
                                </Col>
                                {/* <Col xs={12} sm={2} md={4} lg={6}>  
                                    <Button size="sm" style={{fontSize: ".8rem"}}>
                                        Search
                                    </Button>
                                </Col> */}
                            </FormGroup>  
                        </Form>
                    </Row>                           
                </section>

                <section className="patient-listing-container">
                    <Row>
                        <Table striped hover bordered responsive scrollY size="sm" >
                            <thead>
                                <tr>
                                    <th className="column-id">ID</th>
                                    <th className="column-healthCardNo">Health Card Number</th>   
                                    <th className="column-firstname">First Name</th>
                                    <th className="column-firstname">Last Name</th>
                                    <th className="column-firstname">Home Phone</th>
                                    <th className="column-firstname">Email</th>
                                    <th className="column-firstname">Action</th>

                                </tr>
                            </thead>
                            <tbody>
                                {patientList.length === 0 &&
                                    <tr><td colSpan="4" className="text-center"><i>No listings found</i></td></tr>
                                }
                                {patientList.length > 0 &&
                                    patientList.filter((patient) => {
                                        if (searchOhip === "") {
                                            return patient
                                        } else if(patient.healthcard_no.includes(searchOhip)) {
                                            return patient

                                        }
                                    }).map(patient => 
                                        <tr key={patient.patient_id}>
                                            <td>{patient.patient_id}</td>
                                            <td>{patient.healthcard_no}</td>
                                            <td>{patient.first_name}</td>
                                            <td>{patient.last_name}</td>
                                            <td>{patient.home_phone}</td>
                                            <td>{patient.email}</td>
                                            <td>
                                                <div className="patientViewButton-container">
                                                    <button id="patientViewButton" className="patientViewButton"
                                                        type="button"
                                                        onClick={(e) => patientViewRoute(e, patient)}
                                                        // onClick={PatientEditContactDetails}
                                                    >Select
                                                    </button>  
                                                </div>                                            
                                            </td>

                                        </tr>
                                    )
                                }
                            </tbody>
                        </Table>
                    </Row> 
                </section> 
            </div>   
        </Container>
    )
}

export default Patients
