import React, { useState, useEffect } from "react";
import { NavLink as RouteLink } from "react-router-dom";
import { Container, Form, FormGroup, Label, Input, Row, Col, Table, Button } from 'reactstrap'
import { useParams } from "react-router";
// import "../../css/patientEditContactDetails.css"
import "../styles/patientDemographicsReadOnly.css"


const PatientDemographicsReadOnly = ({patient, handleDemoViewEditClick}) => {

    // // Function to format date 
    // const formatEMRDate = (dob) => {
    //   const formattedDOB = new Date(patient.dob).toLocaleDateString();
    //   return formattedDOB;
    // } 

    //Function to calculate age
    const getAge = () => {

    }

    const getPatientAge = (dob) => {
      // Date example below: patient_id = 4; dob 1975-07-12
     // dob datatype in mySQL: DATETIME
     // patient.DOB ==> '1975-07-12 11:20:20'
     // mySQL pulls patient.DOB as string type
     // current date ==> '2021-12-2'
 
     // Convert patient.dob to date type
     // Return value in this format: 'Sat Jul 12 1975 11:20:20 GMT-0400 (Eastern Daylight Time))
     // In JS, getMonth starts from 0 for Jan then ends at 11 for Dec
     var patientDOB = new Date(dob) 
     var patientDOBYear = patientDOB.getFullYear() // returns year ie. 1975
     var patientDOBMonth = patientDOB.getMonth() + 1 // returns month ie. 07
     var patientDOBDay = patientDOB.getDate() // returns day ie. 12
 
     var currentDt = new Date()
     var currentYr = currentDt.getFullYear() // returns year ie. 2021
     var currentMm = currentDt.getMonth() + 1  // returns month ie. 11 -- mySQL pulling one month off
     var currentDd = currentDt.getDate() // returns day ie. 2
 
     var yrDiff = currentYr - patientDOBYear
     var mnDiff = currentMm - patientDOBMonth
     var ddDiff = currentDd - patientDOBDay
 
     var patientAge = 0
 
     if( mnDiff > 0) patientAge = yrDiff
     if( mnDiff < 0) patientAge = yrDiff - 1
     if ( mnDiff === 0 ) {
       if( ddDiff > 0) patientAge = yrDiff
       if( ddDiff < 0) patientAge = yrDiff - 1
       if( ddDiff = 0) patientAge = yrDiff
     }
     return patientAge
   }


    return (      
      <Container>
           
            <Row>
              <div className="viewDemo-cardTitle-container">
                <Col lg="6" sm="6" xs="12">
                  <div className="viewDemo-header-container">
                       Demographics
                  </div>    
                </Col>
                <Col lg="6" sm="6" xs="12">              
                  <div className="viewDemo-button-container">
                        <Button size="sm" className="viewDemo-edit-button"
                           onClick={(event) => handleDemoViewEditClick(event, patient)}
                        >Edit
                        </Button>
                        
                        <Button size="sm" className="viewDemo-back-button"
                        // onClick={(e) => handleContactDetails_BackButton(e, patient)}
                            id="DemoEditButton"
                            tag={RouteLink}
                            to="/patients"
                        >Back
                        </Button>                          
                  </div>
                </Col>
                </div>
                <br></br> <br></br>
            </Row>

            <Row>
              <div className="viewDemo-container">
                  <Col lg="6" sm="6" xs="12">
                      <div id="viewDemo-mainInfo-column1-container"> 
                          {/* <label id="viewDemo-label" className="viewDemo-label">DOB    :      <strong>{formatEMRDate(patient.dob)}</strong></label> <br></br> */}
                          <label id="viewDemo-label" className="viewDemo-label">DOB    :      <strong>{patient.dob}</strong></label> <br></br>
                          <label id="viewDemo-label" className="viewDemo-label">Age    :      <strong>{patient.age}</strong></label> <br></br> 
                          <label id="viewDemo-label" className="viewDemo-label">Gender    :      <strong>{patient.gender_description}</strong></label> <br></br> 
                          <label id="viewDemo-label" className="viewDemo-label">Marital Status    :      <strong>{patient.marital_status_desc}</strong></label>  <br></br> <br></br>
                          <label id="viewDemo-label" className="viewDemo-label">Race    :      <strong>{patient.race_name}</strong></label> <br></br> 
                          <label id="viewDemo-label" className="viewDemo-label">Preferred Language    :      <strong>{patient.pref_language_name}</strong></label>  
                      </div>
                  </Col>

                  <Col lg="6" sm="6" xs="12">
                      <div id="viewDemo-mainInfo-column2-container">
                      <label id="viewDemo-label" className="viewDemo-label">Employer Name    :      <strong>{patient.employer_name}</strong></label> <br></br>
                          <label id="viewDemo-label" className="viewDemo-label">Work Phone    :      <strong>{patient.work_phone}</strong></label> <br></br> 
                          <label id="viewDemo-label" className="viewDemo-label">Occupation    :      <strong>{patient.occupation}</strong></label>  
                      </div>
                  </Col>   
              </div>    
            </Row>     

  </Container>



    )
}

export default PatientDemographicsReadOnly