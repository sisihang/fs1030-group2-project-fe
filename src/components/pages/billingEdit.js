import { useEffect, useState, React } from "react";
import { useParams } from "react-router";
import {
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  FormText,
  Container,
  Row,
} from "reactstrap";
import { useNavigate, Link } from "react-router-dom";

const BillingEdit = () => {

  const { id } = useParams();
  const patientId = id;
  const navigate = useNavigate();
  const [billing, setBilling] = useState([]);
  const [form, setForm] = useState({ display: "none" });
  const [bill, setBill] = useState({ bill_invoice_id: "", care_provider_id: "", service_rendered: "",  service_date: "",  billing_date: "", due_date: "", total_amount: "", payment: "",  balance: "" ,});
  const [patients, setPatients] = useState([]);


  useEffect(() => {
    const getData = async () => {
        const patientResponse = await fetch(
            `http://localhost:4000/api/patients/${id}`
          );
          const patientData = await patientResponse.json();
          setPatients(patientData);
    };
    getData();
  }, [id]);

  useEffect(() => {
    async function fetchData() {
      const res = await fetch(`http://localhost:4000/billing/${id}`);
      res
        .json()
        .then((res) => setBilling(res))
        .catch((err) => console.log(err));
    }
    fetchData();
  }, [bill]);

  const handleEdit = (event, invoice) => {
    event.preventDefault();
    setForm({ display: "block" });
    setBill(invoice);
    console.log(invoice);
  };

    const handleChange = (event) => {
      setBill((prevState) => ({
        ...prevState,
        [event.target.name]: event.target.value,
      }));
    };

      const handleSubmit =  (event) => {
        event.preventDefault();
        // console.log(id);
         fetch(`http://localhost:4000/billing/${id}`, {
          method: "put",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
          },

          //make sure to serialize your JSON body
          body: JSON.stringify(bill),
        }).then((response) => response.json());
        navigate(`/patients/${patientId}/billing`);
      };



  return (
    <Container style={{ backgroundColor: "#C4A484" }} className="my-5">
     
     <Row style={{ padding: '30px' }}>
                {patients.map((patient) => (
                    <h5>
                        Edit Billing - Name:{" "}
                        {patient.first_name +
                            " " +
                            patient.middle_name +
                            " " +
                            patient.last_name + " "}
                           <br /> Patient ID: {" "}
                            {patient.patient_id}
                    </h5>
                    ))}
            </Row>
<Row className="m-3">
            {billing.map((invoice) => (
             <div key={invoice.id}>
              <p>Bill Invoice No.: &nbsp;{invoice.bill_invoice_id} &nbsp; &nbsp;
               Service Rendered: &nbsp;{invoice.service_rendered} </p>
             <button className="mb-3 px-3" onClick={(e) => handleEdit(e, invoice)}>Edit</button>
            </div>
             ))}
             </Row>
 <Form className="m-3" onSubmit={(e) => handleSubmit(e)} style={form}>
 <FormGroup >
          <Label>Invoice No.:</Label>
          <Input
           type="text"
           name="bill_invoice_id"
           value = {bill.bill_invoice_id}
           disabled />
        </FormGroup>
        <FormGroup >
          <Label>Care Provider</Label>
          <Input
           type="text"
           name="care_provider_id"
           value = {bill.care_provider_id}
           disabled />
        </FormGroup>
        <FormGroup>
          <Label>Service Rendered:</Label>
          <Input
            type="text"
            name="service_rendered"
            value = {bill.service_rendered}
            onChange={handleChange}
          />
        </FormGroup>
        <FormGroup>
          <Label>Service Date:</Label>
          <Input
            type="date"
            name="service_date"
            value = {bill.service_date}
            onChange={handleChange}
          />
        </FormGroup>
        <FormGroup>
          <Label>Billing Date:</Label>
          <Input
            type="date"
            name="billing_date"
            value = {bill.billing_date}
            onChange={handleChange}
            />
        </FormGroup>
        <FormGroup>
          <Label>Due Date:</Label>
          <Input
            type="date"
            name="due_date"
            value = {bill.due_date}
            onChange={handleChange}
          />
        </FormGroup>
        <FormGroup>
          <Label>Total Amount:</Label>
          <Input
            type="text"
            name="total_amount"
            value = {bill.total_amount}
            onChange={handleChange}
           /> 
        </FormGroup>
        <FormGroup>
          <Label>Payment:</Label>
          <Input
            type="text"
            name="payment"
            value = {bill.payment}
            onChange={handleChange}
          />
        </FormGroup>
        <FormGroup>
          <Label>Balance</Label>
          <Input
            type="text"
            name="balance"
            value = {bill.balance}
            onChange={handleChange}
          />
        </FormGroup>
        <Button type="submit" value="submit">Submit</Button>
      </Form>
     
    </Container>
  );
};

export default BillingEdit;
