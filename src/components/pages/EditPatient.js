import React, { useState,useHistory, useEffect} from 'react';
import { Form, FormGroup, Col, Row, Input, Label, Button, Container, CardBody, Card, CardText } from 'reactstrap';
import { useNavigate } from "react-router-dom";
import { useParams } from "react-router";
import axios from "axios";
import'../styles/admin.css';



const EditPatient = () => {

    let navigate = useNavigate(); //The useHistory hook gives you access to the history instance that you may use to navigate.
    const { id } = useParams(); //The useParams() hook helps us to access the URL parameters from a current route.
  
    const [user, setUser] = useState({
      HealthCardno: "",
      HealthCardver: "",
      fname: "",
      mname: "",
      lname: "",
      addressline1: "",
      addressline2: "",
      city: "",
      provincecode: "",
      postalcode: "",
      homephone: "",
      cellphone: "",
      email: "",
      familydoctorcode: "",
    });
    const {
      HealthCardno, HealthCardver, fname, mname, lname, addressline1, addressline2, city, provincecode, postalcode, homephone, cellphone, email, familydoctorcode,
    } = user;
    const onInputChange = (e) => {
      setUser({ ...user, [e.target.name]: e.target.value });
    };
  
    useEffect(() => {
      loadUser();
    }, []);
  
    const updatePatient = async (e) => {
      e.preventDefault();
      await axios.put(`http://localhost:4000/api/v1/patient/${id}`, user);
      navigate("/admin");
    };
  
    const loadUser = () => {
      fetch(`http://localhost:4000/api/v1/patient/${id}`, {
        method: "GET",
      })
        .then((response) => response.json())
        .then((result) => {
          console.log(result);
          setUser({
            id: id,
            update: true,
            healthcardno: result.response[0].healthcard_no,
            healthcardver: result.response[0].healthcard_ver,
            fname: result.response[0].first_name,
            mname: result.response[0].middle_name,
            lname: result.response[0].last_name,
            addressline1: result.response[0].address_line1,
            addressline2: result.response[0].address_line2,
            city: result.response[0].city,
            provincecode: result.response[0].province_code,
            postalcode: result.response[0].postal_code,
            homephone: result.response[0].home_phone,
            cellphone: result.response[0].cell_phone,
            email: result.response[0].email,
            familydoctorcode: result.response[0].family_doctor_code,
          });
        })
        .catch((error) => console.log("error", error));
    };
  
    
    return (
      <div className="container">
        <div className="row mt-4">
          <div className="col-sm-5 col-offset-3 mx-auto shadow p-5">
            <h4 className="text-center mb-4">Edit A Patient</h4>
  
            <h5 className="text-success">Patient ID: {user.id} </h5>
  
            <div className="form-group mb-3">
              <input
                type="text"
                className="form-control form-control-lg"
                placeholder="Enter HealthCard no "
                name="HealthCardno"
                value={HealthCardno}
                onChange={(e) => onInputChange(e)} />
            </div>
  
            <div className="form-group mb-3">
              <input
                type="text"
                className="form-control form-control-lg"
                placeholder="Enter HealthCardver"
                name="HealthCardver"
                value={HealthCardver}
                onChange={(e) => onInputChange(e)} />
            </div>
  
  
            <div className="form-group mb-3">
              <input
                type="text"
                className="form-control form-control-lg"
                placeholder="Enter Patient Name"
                name="fname"
                value={fname}
                onChange={(e) => onInputChange(e)} />
            </div>
  
            <div className="form-group mb-3">
              <input
                type="text"
                className="form-control form-control-lg"
                placeholder="Enter Patient Name"
                name="mname"
                value={mname}
                onChange={(e) => onInputChange(e)} />
            </div>
  
  
            <div className="form-group mb-3">
              <input
                type="text"
                className="form-control form-control-lg"
                placeholder="Enter Patient Name"
                name="lname"
                value={lname}
                onChange={(e) => onInputChange(e)} />
            </div>
  
            <div className="form-group mb-3">
              <input
                type="text"
                className="form-control form-control-lg"
                placeholder="Enter addressline1 Name"
                name="addressline1"
                value={addressline1}
                onChange={(e) => onInputChange(e)} />
            </div>
  
            <div className="form-group mb-3">
              <input
                type="text"
                className="form-control form-control-lg"
                placeholder="Enter addressline2 Name"
                name="addressline2"
                value={addressline2}
                onChange={(e) => onInputChange(e)} />
            </div>
  
            <div className="form-group mb-3">
              <input
                type="text"
                className="form-control form-control-lg"
                placeholder="Enter city"
                name="city"
                value={city}
                onChange={(e) => onInputChange(e)} />
            </div>
  
            <div className="form-group mb-3">
              <input
                type="text"
                className="form-control form-control-lg"
                placeholder="Enter province code"
                name="provincecode"
                value={provincecode}
                onChange={(e) => onInputChange(e)} />
            </div>
  
            <div className="form-group mb-3">
              <input
                type="text"
                className="form-control form-control-lg"
                placeholder="Enter Postal code "
                name="Postalcode"
                value={postalcode}
                onChange={(e) => onInputChange(e)} />
            </div>
  
            <div className="form-group mb-3">
              <input
                type="text"
                className="form-control form-control-lg"
                placeholder="Enter home phone "
                name="homephone"
                value={homephone}
                onChange={(e) => onInputChange(e)} />
            </div>
  
            <div className="form-group mb-3">
              <input
                type="text"
                className="form-control form-control-lg"
                placeholder="Enter Cell phone "
                name="cellphone"
                value={cellphone}
                onChange={(e) => onInputChange(e)} />
            </div>
  
            <div className="form-group mb-3">
              <input
                type="text"
                className="form-control form-control-lg"
                placeholder="Enter your Email"
                name="email"
                value={email}
                onChange={(e) => onInputChange(e)} />
            </div>
  
            <div className="form-group mb-3">
              <input
                type="text"
                className="form-control form-control-lg"
                placeholder="Enter family doctor code"
                name="familydoctorcode"
                value={familydoctorcode}
                onChange={(e) => onInputChange(e)} 
                />
            </div>
          <button onClick={updatePatient} className="btn btn-secondary btn-block">Update Patient</button>
       
       </div>
            
            
          </div>
  
        </div>
    
  
  );
};
 
    
  

    export default EditPatient
   