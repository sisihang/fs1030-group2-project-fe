import React, { useState, useEffect } from "react";
import { useParams } from "react-router";
import { Container, Form, FormGroup, Label, Input, Row, Col, Table, Button, CardBody, CardText, Card } from 'reactstrap'
import Navigation  from '../shared/navigation'
import { useNavigate } from "react-router-dom";


const PatientNotesAdd = () => {
    const { id } = useParams();
    const navigate = useNavigate();

    // Function to format date
    const formatDateToYYYMMDDHHMMSS = (dateFieldToFormat) => {
        const formattedDate = dateFieldToFormat.getFullYear() + '-' + (dateFieldToFormat.getMonth()+1) + '-' + dateFieldToFormat.getDate() +' '+ dateFieldToFormat.getHours()+':'+ dateFieldToFormat.getMinutes()+':'+ dateFieldToFormat.getSeconds();
        return formattedDate;
    } 


    const patientId = id;

    // new Date() returns date in this format 'Thu Dec 02 2021 10:01:17 GMT-0500 (Eastern Standard Time)'
    const currentDate = formatDateToYYYMMDDHHMMSS(new Date()) 
    // formatDateToYYYYMMDDHHMMSS returns date in this format '2021-12-2 10:2:51'


    const [noteAddFormData, setNoteAddFormData] = useState({
        chart_entry_date: currentDate, 
        comments : '', 
        patient_id: patientId, 
        care_provider_id: ''
    })


    const redirectToNoteList = (event) => {
        event.preventDefault();
        navigate(`/patients/${id}/notes`);
    };

   // Handles changes in Add form 
   const handleAddNoteForm_FieldValueChange = (event) => {
    event.preventDefault();
    const labFieldName = event.target.getAttribute("name");
    const labFieldValue = event.target.value;

    const newNoteFormData = { ...noteAddFormData };
    newNoteFormData[labFieldName] = labFieldValue
    setNoteAddFormData(newNoteFormData)
}


    // Handles the SUBMIT button in ADD Form
    const handleAddLabForm_SubmitButton = (event) => {
        event.preventDefault();
        fetch(`http://localhost:4000/api/patient_notes`, {
            method: "post",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
            },
            //make sure to serialize your JSON body
            body: JSON.stringify(noteAddFormData),
          }).then((response) => response.json());
          navigate(`/patients/${patientId}/notes`);
    };   

      
    return(
        <Container style={{ backgroundColor: "#ADD8E6" }} className="my-5">
         <Navigation patientID={id} />  
            <Row className="my-5">
                 <div className="editContactDetails-maincontainer">
                    <Form onSubmit = {handleAddLabForm_SubmitButton}>
                        <Row>
                            <div className="editContactDetails-cardTitle-container">
                            <Col lg="6" sm="6" xs="12">
                                <div className="editContactDetails-header-container">
                                    Notes Input Form
                                </div>    
                            </Col>
                            <Col lg="6" sm="6" xs="12">              
                                <div className="editContactDetails-button-container">
                                    <Button size="sm" className="editContactDetails-save-button"
                                    >
                                        Submit
                                    </Button>

                                    <Button size="sm" className="editContactDetails-submit-button"
                                        onClick={redirectToNoteList}
                                        >Cancel
                                    </Button>                                                                           
                                </div>
                            </Col>
                            </div>
                            <br></br> <br></br>
                        </Row>

                        <Row>
                            <div className="editContactDetails-container">
                                <Col lg="6" sm="6" xs="12">
                                    <div id="editContactDetails-mainInfo-container"> 
                                       
                                        <label id="editContactDetails-label" className="editContactDetails-label">Comments  :</label> 
                                        <textarea id="editContactDetails-textbox" className="editContactDetails-textbox"
                                                    type="text"
                                                    name="comments"
                                                    placeholder="Enter comments"
                                                    onChange={handleAddNoteForm_FieldValueChange}
                                                    style = {{ width: "200%"}}
                                        ></textarea> <br></br> <br></br>    

                                        <label id="editContactDetails-label" className="editContactDetails-label">Care Provider  :</label> 
                                        <input id="editContactDetails-textbox" className="editContactDetails-textbox"
                                                    type="text"
                                                    name="care_provider_id"
                                                    placeholder="Enter care provider"
                                                    onChange={handleAddNoteForm_FieldValueChange}
                                        ></input> <br></br> <br></br> 

                                    </div>
                                </Col>
                            </div>  
                        </Row>    
                    </Form> 
                      
                </div>

            </Row>
            <Card className="text-white bg-secondary my-5 py-4 text-center">
                <CardBody>
                    <CardText>
                        <span className="text-white m-0"></span>
                    </CardText>
                </CardBody>
            </Card>
            
        </Container>
    )
}

export default PatientNotesAdd
