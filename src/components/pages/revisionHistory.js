import { useEffect, useState, React } from "react";
import {
  Container,
  Row,
  Col,
  Button,
  CardBody,
  CardText,
  Card,
  Table,
} from "reactstrap";
import Navigation  from '../shared/navigation'
import { useParams } from "react-router";

const Meds = () => {
  const { id } = useParams();
  const [revisions, setRevisions] = useState([]);

  useEffect(() => {
    const getData = async () => {
      const revResponse = await fetch(
        `http://localhost:4000/revision_history/${id}`
      );
      const revData = await revResponse.json();
      setRevisions(revData);

    };
    getData();
  }, []);

  return (
    <Container style={{ backgroundColor: "#FED8B1" }} className="my-5">
                  <Navigation patientID={id} />  
      <Row style={{ padding: "30px" }}>
        <h3>Revision History</h3>
      </Row>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Date updated</th>
            <th>Field updated</th>
            <th>Updated By</th>
          </tr>
        </thead>
        <tbody>
          {revisions.length > 0 &&
            revisions.map((revision) => (
              <tr>
                <td>{revision.revision_date}</td>
                <td>{revision.column_name}</td>
                <td>{revision.careProvider_name}</td>
              </tr>
            ))}
        </tbody>
      </Table>
      <div className="text-center">
        <Button className>Add New</Button>
      </div>
    </Container>
  );
};

export default Meds;
