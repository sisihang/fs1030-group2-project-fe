import { useEffect, useState, React } from "react";
import { useParams } from "react-router";
import {
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  FormText,
  Container,
  Row,
} from "reactstrap";
import { useNavigate, Link } from "react-router-dom";

const BillingAdd = () => {
  const { id } = useParams();
  const patientId = id;
  const [careProviderId, setCareProviderId] = useState("");
  const [serviceRendered , setServiceRendered] = useState("");
  const [serviceDate, setServiceDate] = useState("");
  const [billingDate, setBillingDate] = useState("");
  const [dueDate, setDueDate] = useState("");
  const [totalAmount, setTotalAmount] = useState("");
  const [payment, setPayment] = useState("");
  const [balance, setBalance] = useState("");
  const [patients, setPatients] = useState([]);


  const navigate = useNavigate();


  useEffect(() => {
    const getData = async () => {
      const patientRes = await fetch(
        `http://localhost:4000/api/patients/${patientId}`
      );

      const res = await patientRes.json();
      setCareProviderId(res[0].family_doctor_code);

    };
    getData();
  }, []);

  useEffect(() => {
    const getData = async () => {
        const patientResponse = await fetch(
            `http://localhost:4000/api/patients/${id}`
          );
          const patientData = await patientResponse.json();
          setPatients(patientData);
          
    };
    getData();
  }, []);

  const formSubmit = async (event) => {
    event.preventDefault();
    const response = await fetch("http://localhost:4000/billing", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        careProviderId,
        patientId,
        serviceRendered,
        serviceDate,
        billingDate,
        dueDate,
        totalAmount,
        payment,
        balance,
       
      }),
    });
    if (response.status === 201) {
      alert(`Congrats! Submission submitted`);
    }
    navigate(`/patients/${patientId}/billing`);
  };

  return (
    <Container style={{ backgroundColor: "#C4A484" }} className="my-5">
      <Form>
     
      <Row style={{ padding: '30px' }}>
                {patients.map((patient) => (
                    <h5>
                        Add Billing - Name:{" "}
                        {patient.first_name +
                            " " +
                            patient.middle_name +
                            " " +
                            patient.last_name + " "}
                           <br /> Patient ID: {" "}
                            {patient.patient_id}
                    </h5>
                    ))}
            </Row>
      
        <FormGroup>
          <Label for="careProviderId">Care Provider</Label>
          <Input
           type="text"
           name="careProviderId"
           id="careProviderId"
           value = {careProviderId}
           disabled />
        </FormGroup>
        <FormGroup>
          <Label for="serviceRendered">Service Rendered</Label>
          <Input
            type="text"
            name="serviceRendered"
            id="serviceRendered"
            placeholder="Service Rendered"
            onChange={(e) => setServiceRendered(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label for="serviceDate">Service Date</Label>
          <Input
            type="date"
            name="serviceDate"
            id="serviceDate"
            placeholder="Service Date"
            onChange={(e) => setServiceDate(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label for="billingDate">Billing Date</Label>
          <Input
            type="date"
            name="billingDate"
            id="billingDate"
            placeholder="Billing Date"
            onChange={(e) => setBillingDate(e.target.value)}
            />
        </FormGroup>
        <FormGroup>
          <Label for="date">Due Date</Label>
          <Input
            type="date"
            name="date"
            id="dueDate"
            placeholder="Due Date"
            onChange={(e) => setDueDate(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label for="totalAmount">Total Amount</Label>
          <Input
            type="text"
            name="totalAmount"
            id="totalAmount"
            placeholder="Total Amount"
            onChange={(e) => setTotalAmount(e.target.value)}
           /> 
        </FormGroup>
        <FormGroup>
          <Label for="payment">Payment</Label>
          <Input
            type="text"
            name="payment"
            id="payment"
            placeholder="Payment"
            onChange={(e) => setPayment(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label for="balance">Balance</Label>
          <Input
            type="text"
            name="balance"
            id="balance"
            placeholder="Balance"
            onChange={(e) => setBalance(e.target.value)}
          />
        </FormGroup>
        
      </Form>
      <div style={{ padding: "10px", textAlign: "center" }}>
        <Button onClick={formSubmit}>Submit</Button>
       
      </div>
    </Container>
  );
};

export default BillingAdd;
