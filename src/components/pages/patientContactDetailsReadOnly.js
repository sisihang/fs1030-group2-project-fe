import React, { useState, useEffect } from "react";
import { NavLink as RouteLink } from "react-router-dom";
import { Container, Form, FormGroup, Label, Input, Row, Col, Table, Button } from 'reactstrap'
import { useParams } from "react-router";
// import "../../css/patientEditContactDetails.css"
import "../styles/patientContactDetailsReadOnly.css"




const PatientContactDetailsReadOnly = ({patient, handlePatientViewEditClick}) => {
    return (      
      <Container>
      {/* <section className="patient-listing-container"> */}
           {/* <div key={patient.patient_id}>  */}
            {/* <Row>
              <div className="contactDetailsReadOnly-PatientInfo-header-container">
                   <div className="contactDetailsReadOnly-InfoTitle-container">
                      <label id="contactDetailsReadOnly-label" className="contactDetailsReadOnly-label">Health Card Number:   {patient.healthcard_no + "-" + patient.healthcard_ver}</label> 
                      <label className="viewPatient-patientIdlabel">Patient ID:   {patient.patient_id}</label> <br></br>
                      <label className="viewPatient-patientNamelabel">Patient Name:   {patient.patient_fname + " " + patient.patient_mname + " " + patient.patient_lname}</label>                              
                      <label className="viewPatient-doctorNamelabel">Current Care Provider:   {patient.doctor_fname + " " + patient.doctor_mname + " " + patient.doctor_lname}</label>
                  </div>
              </div>
            </Row> */}
            <Row>
              <div className="contactDetailsReadOnly-cardTitle-container">
                <Col lg="6" sm="6" xs="12">
                  <div className="contactDetailsReadOnly-header-container">
                        Patient Contact Information
                  </div>    
                </Col>
                <Col lg="6" sm="6" xs="12">              
                  <div className="contactDetailsReadOnly-button-container">
                        <Button size="sm" className="contactDetailsReadOnly-edit-button"
                           onClick={(event) => handlePatientViewEditClick(event, patient)}
                        >Edit
                        </Button>
                        
                        <Button size="sm" className="contactDetailsReadOnly-back-button"
                        // onClick={(e) => handleContactDetails_BackButton(e, patient)}
                        id="CreditDetailsEdit"
                        tag={RouteLink}
                        to="/patients"
                        >Back
                        </Button>                          
                  </div>
                </Col>
                </div>
                <br></br> <br></br>
            </Row>

            <Row>
              <div className="contactDetailsReadOnly-container">
                  <Col lg="6" sm="6" xs="12">
                      <div id="contactDetailsReadOnly-mainInfo-column1-container"> 
                          <label id="contactDetailsReadOnly-label" className="contactDetailsReadOnly-label">Patient name    :      <strong>{patient.first_name + " " + patient.middle_name + " " + patient.last_name}</strong></label> 
                          <label id="contactDetailsReadOnly-label" className="contactDetailsReadOnly-label">Address    :      <strong>{patient.address_line1 + " " + patient.address_line2 + " " + patient.city + " " + patient.province_code + " " + patient.postal_code}</strong></label>  
                      </div>
                  </Col>

                  <Col lg="6" sm="6" xs="12">
                      <div id="contactDetailsReadOnly-mainInfo-column2-container">
                              <label id="contactDetailsReadOnly-label" className="contactDetailsReadOnly-label">Home Phone    :      <strong>{patient.home_phone}</strong></label> <br></br>
                              <label id="contactDetailsReadOnly-label" className="contactDetailsReadOnly-label">Cell Phone    :      <strong>{patient.cell_phone}</strong></label> <br></br>
                              <label id="contactDetailsReadOnly-label" className="contactDetailsReadOnly-label">Email    :      <strong>{patient.email}</strong></label> 
                      </div>
                  </Col>   
              </div>    
            </Row>     

            <Row>
            
            </Row>   
          {/* </div>               */}
        {/* ))} */}

      

       


      {/* </section>    */}
  </Container>



    )
}

export default PatientContactDetailsReadOnly
