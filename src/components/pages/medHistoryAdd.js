import { useEffect, useState, React } from "react";
import { useParams } from "react-router";
import {
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  FormText,
  Container,
  Row,
} from "reactstrap";
import { useNavigate, Link } from "react-router-dom";

const MedHistoryAdd = () => {
  const { id } = useParams();
  const patientId = id;
  const [careProviderId, setCareProviderId] = useState("");
  
  const [medHistoryDate, setMedHistoryDate] = useState("");
  const [majIllness, setMajIllness] = useState("");
  const [surgProcedure, setSurgProcedure] = useState("");
  const [comments, setComments] = useState("");
  const [patients, setPatients] = useState([]);

  const navigate = useNavigate();

  useEffect(() => {
    const getData = async () => {
      const patientRes = await fetch(
        `http://localhost:4000/api/patients/${patientId}`
      );
      const res = await patientRes.json();
      setCareProviderId(res[0].family_doctor_code);
     
       
    };
    getData();
  }, []);

  useEffect(() => {
    const getData = async () => {
        const patientResponse = await fetch(
            `http://localhost:4000/api/patients/${id}`
          );
          const patientData = await patientResponse.json();
          setPatients(patientData);
          
    };
    getData();
  }, []);

  const formSubmit = async (event) => {
    event.preventDefault();
    const response = await fetch("http://localhost:4000/medical_history", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        patientId,
        careProviderId,
        medHistoryDate,
        majIllness,
        surgProcedure,
        comments,
      }),
    });
    if (response.status === 201) {
      alert(`Congrats! Submission submitted`);
    }
    navigate(`/patients/${patientId}/medHistory`);
  };

  return (
    <Container style={{ backgroundColor: "rgb(162, 252, 252)" }} className="my-5">
      <Form>

       <Row style={{ padding: '30px' }}>
                {patients.map((patient) => (
                    <h5>
                        Medical History - Name:{" "}
                        {patient.first_name +
                            " " +
                            patient.middle_name +
                            " " +
                            patient.last_name + " "}
                           <br /> Patient ID: {" "}
                            {patient.patient_id}
                    </h5>
                    ))}
            </Row>
            
        <FormGroup>
          <Label for="careProviderId">Care Provider</Label>
          <Input
            type="text"
            name="careProviderId"
            id="careProviderId"
            value = {careProviderId}
           disabled
            />
        </FormGroup>
        <FormGroup>
          <Label for="medHistoryDate">Date</Label>
          <Input
            type="date"
            name="medHistoryDate"
            id="medHistoryDate"
            placeholder="Date"
            onChange={(e) => setMedHistoryDate(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label for="majIllness">Major Illness</Label>
          <Input
            type="text"
            name="majIllness"
            id="majIllness"
            placeholder="Major Illness"
            onChange={(e) => setMajIllness(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label for="surgProcedure">Surgical Procedure</Label>
          <Input
            type="text"
            name="surgProcedure"
            id="surgProcedure"
            placeholder="Surgical Procedure"
            onChange={(e) => setSurgProcedure(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label for="comments">Comment</Label>
          <Input
            type="textarea"
            name="comments"
            id="comments"
            placeholder="Comments"
            onChange={(e) => setComments(e.target.value)}
          />
        </FormGroup>
       
      </Form>
      <div style={{ padding: "10px", textAlign: "center" }}>
        <Button onClick={formSubmit}>Submit</Button>
       
      </div>
    </Container>
  );
};

export default MedHistoryAdd;
