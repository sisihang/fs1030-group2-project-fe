import React, { useState, useEffect } from "react";
import { NavLink as RouteLink } from "react-router-dom";
import { Container, Form, FormGroup, Label, Input, Row, Col, Table, Button } from 'reactstrap'
import { useParams } from "react-router";
import "../styles/patientEmergencyReadOnly.css"
import { useNavigate } from 'react-router-dom';


const PatientEmergencyReadOnly = ({ currentPatientID, selectedContactID, editEmergencyMode, handleEmergencyViewEditClick}) => {

    const { id } = useParams();
    const patientID = id;

 

    const navigate = useNavigate();

    const [emergencyList, setEmergencyList] = useState([])
    const [searchNote, setSearchNote] = useState([])
    const [addContactEntryForm, setAddContactEntryForm] = useState({ display: "none" });
    const [addContactButton, setaAdContactButton] = useState({ visibility: "visible"});

    // States used in adding new contact
    const [contact_fname, setContact_fname] = useState("")
    const [contact_lname, setContact_lname] = useState("")
    const [contact_phone, setContact_phone] = useState("")
    const [relationship, setRelationship] = useState("")



    useEffect(() => {
        const fetchData = async () => {
          const emergRes = await fetch(`http://localhost:4000/api/patient_emergency/${patientID}`);
          const emergData = await emergRes.json();
          setEmergencyList(emergData);
        };
        fetchData();
      }, [patientID]);    

        
    // Handles the first name input in the Contact ADD form
    const handleContactFirstName = (e) => {
        setContact_fname(e.target.value)
        // setErrorMessages(!errorMessages)        
    }

      // Handles the last name input in the Contact ADD form
    const handleContactLastName = (e) => {
        setContact_lname(e.target.value)
        // setErrorMessages(!errorMessages)        
    }

     // Handles the phone input in the Contact ADD form
     const handleContactPhone = (e) => {
        setContact_phone(e.target.value)
        // setErrorMessages(!errorMessages)        
    }


     // Handles the relationship input in the Contact ADD form
     const handleContactRelationship= (e) => {
        setRelationship(e.target.value)
        // setErrorMessages(!errorMessages)        
    }

    // Handles the edit button in Emergency section in the Patient View page
    const emergencyEditFormRoute = (event, emergItem) => {
        event.preventDefault();
        // /patients/:id/emergency/edit/contactID
        navigate(`/patients/${patientID}/emergency/edit/${emergItem.contact_id}`);        
      }

    // Handles the DELETE button in the CONTACT list
    const handleEmergencyList_DeleteButton = (event, contactIdToDelete) => {
        event.preventDefault();

        if (window.confirm("This will permanently remove this contact.")) {

            fetch(`http://localhost:4000/api/patient_emergency/${contactIdToDelete}`, {
                method: "delete",
                headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                },
            }).then((response) => response.json());

            const newUsers = [...emergencyList]
            const index = newUsers.findIndex((contact, index) => contact.contact_id === contactIdToDelete)

            newUsers.splice(index, 1)
        
            setEmergencyList(newUsers)

        }
    }      

    // dito
    // used to handle the ADD BUTTON click in the Contact List
    const handleEmergencyContactAddButton = (e) => {
        setAddContactEntryForm({display: "block"})
        setaAdContactButton({visibility: "hidden"})
    }    

    const addContactFormSubmit2 = async event => {
        alert('Im here')
        alert(patientID)
    }

    // Used to handle the SUBMIT button in the CONTACT ADD form
    const addContactFormSubmit = async event => {
        event.preventDefault()

        alert(patientID)
        // let foundErrors = userFields_Validation()
 
        // if ( foundErrors.length === 0 ) {   
            const response = await fetch('http://localhost:4000/api/patient_emergency', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    // 'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify({contact_fname, contact_lname, contact_phone, relationship, patientID})
            })

            const payload = await response.json()
            if (response.status >= 400) {
                alert(`Oops! Error: ${payload.message} for fields: ${payload}`)
            } else {
                getContactData_AfterAdd()

                alert('SUBMITTED: New user request has been submitted successfully!')
                setaAdContactButton({visibility: "visible"})
                setAddContactEntryForm({display: "none"})
            }
        // } else {
        //     setErrorMessages( `Validation Error: ${foundErrors.join(" / ")}` )
        // }
    }

    // Used to refresh the screen after adding a record
    const getContactData_AfterAdd = async () => {
        const response = await fetch('http://localhost:4000/api/patient_emergency/${patientID}/${selectedContactID}', {
            method: 'GET',
            headers: {
                // 'Authorization': `Bearer ${token}`
            }
        })
        const data = await response.json()
        setEmergencyList(data)
    }


    return (      
      <Container>
        <div className="emergencyRO-main-container">  
           
            <Row>
              <div className="emergencyRead-cardTitle-container">
                <Col lg="6" sm="6" xs="12">
                  <div className="emergencyRead-header-container">
                       Emergency Contact
                  </div>    
                </Col>
                <Col lg="6" sm="6" xs="12">              
                  <div className="emergencyRead-button-container">
                        <Button size="sm" className="viewEmergency-edit-button"
                            onClick={(event) => handleEmergencyContactAddButton(event)}
                            style={addContactButton}
                        >Add
                        </Button>
                          
                        
                        <Button size="sm" className="emergencyRead-back-button"
                        // onClick={(e) => handleContactDetails_BackButton(e, patient)}
                                id="emergencyEditButton"
                                style={addContactButton}
                                tag={RouteLink}
                                to="/patients"
                            >Back
                        </Button>                          
                  </div>
                 </Col>
                </div>
                <br></br> <br></br>
            </Row>
            <Row>  
                {/*  DITO */}
                <section className="addContact-container">
                    <Form style={addContactEntryForm} onSubmit={addContactFormSubmit2} noValidate>                        
                        <FormGroup row fluid>
                            <div className="contactAddForm-main-container">
                                <Col xs={3} sm={3} md={2} lg={2}>
                                    <div className="inputContactFirstName-container">
                                        <input 
                                            className="contactAdd-input-fields"
                                            type="text" 
                                            name="contact_fname" 
                                            id="firstNameEntry" 
                                            placeholder="First name" 
                                            required 
                                            autoComplete="false"
                                            value={contact_fname} 
                                            onChange={handleContactFirstName}
                                        />
                                    </div>   
                                </Col>
                                <Col xs={3} sm={3} md={2} lg={2}>
                                    <div className="inputContactLastName-container">
                                        <input 
                                            className="contactAdd-input-fields"
                                            type="text" 
                                            name="contact_lname" 
                                            id="lastNameEntry" 
                                            placeholder="Last name" 
                                            required 
                                            autoComplete="false"
                                            value={contact_lname} 
                                            onChange={handleContactLastName}
                                        />
                                    </div>   
                                </Col>                                
                                <Col xs={3} sm={3} md={2} lg={2}>
                                    <div className="inputContactPhone-container">
                                        <input 
                                        className="contactAdd-input-fields"
                                            type="text" 
                                            name="contact_phone" 
                                            id="phoneEntry" 
                                            placeholder="Phone number" 
                                            required 
                                            autoComplete="false"
                                            value={contact_phone} 
                                            onChange={handleContactPhone}
                                        />
                                    </div>   
                                </Col>        
                                <Col xs={3} sm={3} md={4} lg={4}>
                                    <div className="inputContactRelationship-container">
                                        <input 
                                            className="contactAdd-input-fields"
                                            type="text" 
                                            name="relationship" 
                                            id="relationshipEntry" 
                                            placeholder="Relationship" 
                                            required 
                                            autoComplete="false"
                                            value={relationship} 
                                            onChange={handleContactRelationship}
                                        />
                                    </div>   
                                </Col>    
                                {/* <Col xs={4} sm={3} md={1} lg={1}>         
                                    <div className="aaddEmergency-submit-button">            
                                        <Button 
                                            className="addEmergency-submit-button"
                                            id="submitUser-button" 
                                            color="secondary" 
                                            size="sm"
                                        >
                                        Submit
                                        </Button>
                                    </div>
                                </Col>       
                                <Col xs={4} sm={3} md={1} lg={1}>         
                                    <div className="addEmergency-cancel-button">            
                                        <Button 
                                            className="addEmergency-cancel-button"
                                            id="cancelUser-button" 
                                            color="secondary" 
                                            size="sm"
                                            onClick={(e) => handleCancelUserForm(e,)}
                                        >
                                        Cancel
                                        </Button>
                                    </div>
                                </Col>                                                                           */}
                                <Col lg="6" sm="6" xs="12">              
                                        <div className="emergencyAddContact-buttons-container">
                                            <Button size="sm" 
                                                className="addEmergency-submit-button"
                                                id="submitUser-button" 
                                                onClick={(event) => handleEmergencyContactAddButton(event)}
                                            >
                                                Submit
                                            </Button>
                                            
                                            
                                            <Button size="sm" 
                                                    className="addEmergency-cancel-button"
                                                    // onClick={(e) => handleCancelUserForm(e,)}
                                            >
                                                Cancel
                                            </Button>                          
                                        </div>
                                </Col>                            
                            </div>
                        </FormGroup>  
                    </Form>

                    {/* {errorMessages && <p id="errorMessages">{ errorMessages}</p>} */}

                </section>
            </Row>            

            <Row>
                <div className="emergencyRO-table-container">
                    <Table striped hover bordered responsive scrollY size="sm" >
                        <thead>
                            <tr>
                                <th className="emergencyROColumn-id">ID</th>
                                <th className="emergencyROColumn-firstName">First Name</th>   
                                <th className="emergencyROColumn-lastName">Last Name</th>
                                <th className="emergencyROlumn-contactNumber">Contact Number</th>
                                <th className="emergencyRolumn-relationship">Relationship</th>
                                <th className="emergencyROlumn-action">Action</th>

                            </tr>
                        </thead>
                        <tbody>
                        {emergencyList.length === 0 &&
                                    <tr><td colSpan="4" className="text-center"><i>There is no notes found for this patient!</i></td></tr>
                                }
                                {emergencyList.length > 0 &&
                                    emergencyList.filter((emergItem) => {
                                        if (searchNote === "") {
                                            return emergItem
                                        } else if(emergItem.contact_fname.includes(searchNote)) {
                                            return emergItem

                                        }
                                    }).map(emergItem => 
                                    <tr key={emergItem.contact_id}>
                                        <td>{emergItem.contact_id}</td>
                                        <td>{emergItem.contact_fname}</td>
                                        <td>{emergItem.contact_lname}</td>
                                        <td>{emergItem.contact_phone}</td>
                                        <td>{emergItem.relationship}</td>
                                        <td>
                                            <div className="emergencyRO-button-container">
                                                <button id="emergencyRO-button"
                                                            className="labViewButton"
                                                            type="button"
                                                            onClick={(e) => handleEmergencyViewEditClick(e, emergItem.contact_id)}
                                                >Edit
                                                </button>                                                       
                                            </div>     
                                            <div className="emergencyRO-button-container">
                                                <button id="emergencyRO-button" className="emergencyRO-button"
                                                    type="button"
                                                    onClick={(event) => handleEmergencyList_DeleteButton(event, emergItem.contact_id)}
                                                >
                                                    Delete
                                                </button>                                                       
                                            </div>                                                                                     
                                        </td>
                                    </tr>
                                                                
                                )
                            }
                        </tbody>
                    </Table>       
                </div>
            </Row>   
        </div>                      
     </Container>
    )
}

export default PatientEmergencyReadOnly