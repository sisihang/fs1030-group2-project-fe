import React from 'react';
import { useEffect, useState, } from "react";
import { Link, useNavigate } from 'react-router-dom';
import { Container, Row, Col, Button, CardBody, CardText, Card, Table } from 'reactstrap';
import '../styles/radio.css';
import Navigation  from '../shared/navigation'
import { useParams } from "react-router";


const Radio = (props) => {
    const { id } = useParams();
    
    const [radiology, setRadiology] = useState([]);
    const [patients, setPatients] = useState([]);

    const navigate = useNavigate();
  
    const handleDelete = (event, rad) => {
      event.preventDefault();
      fetch(
        `http://localhost:4000/radiology/${rad.radiograph_id}`,
        {
          method: "delete",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
          },
        }
      ).then((response) => response.json());
      window.location.reload(false);
    };

    useEffect(() => {
        const getData = async () => {
            const radioResponse = await fetch(
              `http://localhost:4000/radiology/${id}`
            );
            const radioData = await radioResponse.json();
            setRadiology(radioData);

            const patientResponse = await fetch(
                `http://localhost:4000/api/patients/${id}`
              );
              const patientData = await patientResponse.json();
              setPatients(patientData);
        };
        getData();
      }, []);

      const redirectAdd = (event) => {
        event.preventDefault();
        navigate(`/patients/${id}/radiology/add`);
      };

      const redirectEdit = (event) => {
        event.preventDefault();
        navigate(`/patients/${id}/radiology/edit`);
        
      };
    // const radiologyViewRoute = (event, radio) => {
    //     event.preventDefault();
    //     const path = `/radiology/${radio.radiograph_id}`;
    //     // history.push(path);
    //    navigate(path);
    // }; 

    return(
        <Container style={{ backgroundColor: "#7eb26d" }} className="my-5">
            <Navigation patientID={id} />  
            
            <Row style={{ padding: '30px' }}>
                {patients.map((patient) => (
                    <h5>
                        Radiology - Name:{" "}
                        {patient.first_name +
                            " " +
                            patient.middle_name +
                            " " +
                            patient.last_name + " "}
                           <br /> Patient ID: {" "}
                            {patient.patient_id}
                    </h5>
                    ))}
            </Row>

            <Table striped bordered hover >
        <thead>
          <tr>
            <th>Radiograph Id</th>
            <th>Image</th>
            <th>Image Type</th>
            <th>Care Provider ID</th>
            <th>Radiographer</th>
            <th>Date</th>
            <th>Clinic Name</th>
            <th>Comments</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
         {radiology.length > 0 && 
         radiology.map((rad) => (
           <tr key={rad.radiograph_id}>
                <td>{rad.radiograph_id}</td>
                <td><img src={rad.radio_image} alt="" width="150" height="150" className="img.thumbnail" /></td>
                <td>{rad.image_type}</td>
                <td>{rad.care_provider_id}</td>
                <td>{rad.radiographer}</td>
                <td>{rad.radio_date}</td>
                <td>{rad.clinic_name}</td>
                <td>{rad.comments}</td>
                <td>
                  {" "}
                  <Button
                    onClick={(e) => {
                      handleDelete(e, rad);
                    }}
                    size="sm"
                  >
                    Delete
                  </Button>
                </td>
                 
              </tr>
            ))}
        </tbody>    
        
</Table>

<div className="text-center">
        <span className="px-3"><Button onClick={redirectAdd}>Add</Button></span>
        <Button onClick={redirectEdit}>Edit</Button> 
      </div>     
           
            
        </Container>
    )
}

export default Radio