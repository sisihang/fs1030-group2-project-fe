import React, { useState, useEffect } from "react";
import { useParams } from "react-router";
import { Container, Form, FormGroup, Label, Input, Row, Col, Table, Button, CardBody, CardText, Card } from 'reactstrap'
import Navigation  from '../shared/navigation'
import PatientLab from './patientLab';
import { useNavigate } from "react-router-dom";
// import "react-datepicker/dist/react-datepicker.css";


const PatientLabEdit = () => {
    const { id, labID } = useParams();
    const navigate = useNavigate();

    // const [startDate, setStartDate] = useState(new Date());

    const patientId = id;
    const patientLabID = labID;


    const [labEditFormData, setLabEditFormData] = useState({
        lab_date: '', 
        laboratory: '', 
        lab_code: '',
        technician : '', 
        result: '', 
        comments: '', 
        patient_id: id,
        lab_order_by: ''
    })

    useEffect(() => {
        const fetchData = async () => {
          const patientRes = await fetch(
            `http://localhost:4000/api/patient_laboratory/${patientId}/${patientLabID}`
          );
          const res = await patientRes.json();
        //   setLabEditFormData(res[0].family_doctor_code);
             setLabEditFormData(res[0]);
        };
        fetchData();
      }, [patientId, patientLabID]);
    

   // Handles inputs in Patient Lab Edit form 
   const handleEditLabForm_FieldValueChange = (event) => {
        event.preventDefault();
        const labFieldName = event.target.getAttribute("name");
        const labFieldValue = event.target.value;
        const newlabFormData = { ...labEditFormData };
        newlabFormData[labFieldName] = labFieldValue
        setLabEditFormData(newlabFormData)
    }   

    // Handles the Submit button in EDIT FORM
    const handleEditLabForm_SubmitButton = (event) => {
        event.preventDefault();
        fetch(`http://localhost:4000/api/patient_laboratory/${patientLabID}`, {
            method: "put",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
            },
            //make sure to serialize your JSON body
            body: JSON.stringify(labEditFormData),
          }).then((response) => response.json());
          navigate(`/patients/${patientId}/lab`);
    };  

    
    const redirectToLabList = (event) => {
        event.preventDefault();
        navigate(`/patients/${id}/lab`);
    };

    return(
        <Container style={{ backgroundColor: "#ADD8E6" }} className="my-5">
         <Navigation patientID={id} />  
            <Row className="my-5">
                 <div className="editContactDetails-maincontainer">
                    <Form onSubmit = {handleEditLabForm_SubmitButton}>
                        <Row>
                            <div className="editContactDetails-cardTitle-container">
                            <Col lg="6" sm="6" xs="12">
                                <div className="editContactDetails-header-container">
                                    Update Patient Laboratory
                                </div>    
                            </Col>
                            <Col lg="6" sm="6" xs="12">              
                                <div className="editContactDetails-button-container">
                                    <Button size="sm" className="editContactDetails-save-button"
                                        // onClick={formSubmit}
                                    >Submit
                                    </Button>

                                    <Button size="sm" className="editContactDetails-submit-button"
                                        onClick={redirectToLabList}
                                        >Cancel
                                    </Button>                                                                           
                                </div>
                            </Col>
                            </div>
                            <br></br> <br></br>
                        </Row>

                        <Row>
                            <div className="editContactDetails-container">
                                <Col lg="6" sm="6" xs="12">
                                    <div id="editContactDetails-mainInfo-container"> 
                                        <label id="editContactDetails-label" className="editContactDetails-label">Lab Date   :</label> 
                                        <input id="editContactDetails-textbox" className="editContactDetails-textbox"
                                            type="date"
                                            name="lab_date"
                                            required="required"
                                            value={labEditFormData.lab_date}
                                            onChange={handleEditLabForm_FieldValueChange}
                                        ></input> <br></br>

                                        {/* Should be dropdown */}
                                        <label id="editContactDetails-label" className="editContactDetails-label">Laboratory Type  :   </label> 
                                        <input id="editContactDetails-textbox" className="editContactDetails-textbox" 
                                            type="text"
                                            name="lab_code"
                                            required="required"
                                            onChange={handleEditLabForm_FieldValueChange}                             
                                            value={labEditFormData.lab_code}
                                        ></input> <br></br>

                                        <label id="editContactDetails-label" className="editContactDetails-label">Result  :</label> 
                                        <input id="editContactDetails-textbox" className="editContactDetails-textbox"
                                                    type="text"
                                                    name="result"
                                                    required="required"
                                                    value={labEditFormData.result}
                                                    onChange={handleEditLabForm_FieldValueChange}
                                        ></input> <br></br> <br></br>    

                                        <label id="editContactDetails-label" className="editContactDetails-label">Comments  :</label> 
                                        <input id="editContactDetails-textbox" className="editContactDetails-textbox"
                                                type="text"
                                                name="comments"
                                                required="required"
                                                value={labEditFormData.comments}
                                                onChange={handleEditLabForm_FieldValueChange}
                                        ></input> <br></br>   

                                        <label id="editContactDetails-label" className="editContactDetails-label">Laboratory  :</label> 
                                        <input id="editContactDetails-textbox" className="editContactDetails-textbox"
                                                type="text"
                                                name="laboratory"
                                                required="required"
                                                onChange={handleEditLabForm_FieldValueChange}
                                                value={labEditFormData.laboratory}
                                        ></input> <br></br>    

                                        <label id="editContactDetails-label" className="editContactDetails-label">Technician Name  :</label> 
                                        <input id="editContactDetails-textbox" className="editContactDetails-textbox"
                                                type="text"
                                                name="technician"
                                                required="required"
                                                value={labEditFormData.technician}
                                                onChange={handleEditLabForm_FieldValueChange}                                                
                                        ></input> <br></br>  
                                    
                                        <label id="editContactDetails-label" className="editContactDetails-label">Lab Ordered By  :</label> 
                                        <input id="editContactDetails-textbox" className="editContactDetails-textbox"
                                                type="text"
                                                name="lab_order_by"
                                                required="required"
                                                value={labEditFormData.lab_order_by}                                                
                                                onChange={handleEditLabForm_FieldValueChange}
                                        ></input> <br></br>  <br></br> 
                                    </div>
                                </Col>

                            
                            </div>  
                        </Row>    
                    </Form> 
                      
                </div>
            </Row>
            <Card className="text-white bg-secondary my-5 py-4 text-center">
                <CardBody>
                    <CardText>
                        <span className="text-white m-0"></span>
                    </CardText>
                </CardBody>
            </Card>
            
        </Container>
    )
}

export default PatientLabEdit