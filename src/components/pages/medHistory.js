import React from 'react';
import { useEffect, useState, } from "react";
import { Link, useNavigate } from 'react-router-dom';
import { Container, Row, Col, Button, CardBody, CardText, Card, Table } from 'reactstrap';
import '../styles/radio.css';
import Navigation  from '../shared/navigation'
import { useParams } from "react-router";


const MedHistory = (props) => {
    const { id } = useParams();
    
    const [medicalHistory, setMedicalHistory] = useState([]);
    const [patients, setPatients] = useState([]);

    const navigate = useNavigate();
  
    const handleDelete = (event, history) => {
      event.preventDefault();
      fetch(
        `http://localhost:4000/medical_history/${history.medical_history_id}`,
        {
          method: "delete",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
          },
        }
      ).then((response) => response.json());
      window.location.reload(false);
    };

    useEffect(() => {
        const getData = async () => {
            const medicalResponse = await fetch(
              `http://localhost:4000/medical_history/${id}`
            );
            const medicalData = await medicalResponse.json();
            setMedicalHistory(medicalData);

            const patientResponse = await fetch(
                `http://localhost:4000/api/patients/${id}`
              );
              const patientData = await patientResponse.json();
              setPatients(patientData);
        };
        getData();
      }, []);

      const redirectAdd = (event) => {
        event.preventDefault();
        navigate(`/patients/${id}/medHistory/add`);
      };

      const redirectEdit = (event) => {
        event.preventDefault();
        navigate(`/patients/${id}/medHistory/edit`);
        
      };
    // const radiologyViewRoute = (event, radio) => {
    //     event.preventDefault();
    //     const path = `/radiology/${radio.radiograph_id}`;
    //     // history.push(path);
    //    navigate(path);
    // }; 

    return(
        <Container style={{ backgroundColor: "aquamarine" }} className="my-5">
            <Navigation patientID={id} />  
            
            <Row style={{ padding: '30px' }}>
                {patients.map((patient) => (
                    <h5>
                        Medical History - Name:{" "}
                        {patient.first_name +
                            " " +
                            patient.middle_name +
                            " " +
                            patient.last_name + " "}
                           <br /> Patient ID: {" "}
                            {patient.patient_id}
                    </h5>
                    ))}
            </Row>

            <Table striped bordered hover >
        <thead>
          <tr>
            <th>Medical History ID</th>
           
            <th>Care Provider ID</th>
            <th>Date</th>
            <th>Major Illness</th>
            <th>Surgical Procedures</th>
            <th>Comments</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
         {medicalHistory.length > 0 && 
         medicalHistory.map((history) => (
           <tr key={history.Medical_history_id}>
                <td>{history.medical_history_id}</td>
               
                <td>{history.care_provider_id}</td>
                <td>{history.medical_history_date}</td>
                <td>{history.major_illness}</td>
                <td>{history.surgical_procedure}</td>
                <td>{history.comments}</td>
                <td>
                  {" "}
                  <Button
                    onClick={(e) => {
                      handleDelete(e, history);
                    }}
                    size="sm"
                  >
                    Delete
                  </Button>
                </td>
              </tr>
            ))}
        </tbody>    
        
</Table>

<div className="text-center">
        <span className="px-3"><Button onClick={redirectAdd}>Add</Button></span>
        <Button onClick={redirectEdit}>Edit</Button> 
      </div>     
           
            
        </Container>
    )
}

export default MedHistory