import { useEffect, useState, React } from "react";
import { useParams } from "react-router";
import {
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  FormText,
  Container,
  Row,
} from "reactstrap";
import { useNavigate } from "react-router-dom";

const MedsAdd = () => {
  const { id } = useParams();
  const patientId = id;
  const [medicationName, setMedicationName] = useState("");
  const [instruction, setInstruction] = useState("");
  const [careProviderId, setCareProviderId] = useState("");

  const navigate = useNavigate();
  useEffect(() => {
    const getData = async () => {
      const patientRes = await fetch(
        `http://localhost:4000/api/patients/${patientId}`
      );
      const res = await patientRes.json();
      setCareProviderId(res[0].family_doctor_code);
    };
    getData();
  }, []);

  const formSubmit = async (event) => {
    event.preventDefault();
    const response = await fetch("http://localhost:4000/patient_medication", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        medicationName,
        instruction,
        patientId,
        careProviderId,
      }),
    });
    if (response.status === 201) {
      alert(`Congrats! Submission submitted`);
    }
    navigate(`/patients/${patientId}/meds`);
  };

  return (
    <Container style={{ backgroundColor: "#B19CD9" }} className="my-5">
      <Form>
        <h3 style={{ padding: "10px", textAlign: "center" }} className="font">
          Add a new entry
        </h3>
        <FormGroup>
          <Label for="allergyname">Medication Name</Label>
          <Input
            type="text"
            name="medicationName"
            id="medicationName"
            placeholder="Medication Name"
            onChange={(e) => setMedicationName(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label for="serverity">Instruction</Label>
          <Input
            type="textarea"
            name="instruction"
            id="instruction"
            placeholder="Instruction"
            onChange={(e) => setInstruction(e.target.value)}
          />
        </FormGroup>
      </Form>
      <div style={{ padding: "10px", textAlign: "center" }}>
        <Button onClick={formSubmit}>Submit</Button>
      </div>
    </Container>
  );
};

export default MedsAdd;
