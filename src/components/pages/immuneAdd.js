import { useEffect, useState, React } from "react";
import { useParams } from "react-router";
import {
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  FormText,
  Container,
  Row,
} from "reactstrap";
import { useNavigate } from "react-router-dom";

const ImmuneAdd = () => {
  const { id } = useParams();
  const patientId = id;
  const [immuName, setimmuName] = useState("");
  const [dose, setDose] = useState("");
  const [date, setDate] = useState("");
  const [comment, setComment] = useState("");
  const [administrator, setAdministrator] = useState("");
  const [careProviderId, setCareProviderId] = useState("");
  const [careProviderFname, setCareProviderFname] = useState("");
  const [careProviderMname, setCareProviderMname] = useState("");
  const [careProviderLname, setCareProviderLname] = useState("");


  const navigate = useNavigate();
  useEffect(() => {
    const getData = async () => {
      const patientRes = await fetch(
        `http://localhost:4000/api/patients/${patientId}`
      );
      const res = await patientRes.json();
      setCareProviderId(res[0].family_doctor_code);
      setCareProviderFname(res[0].care_provider_fname)
      setCareProviderMname(res[0].care_provider_mname)
      setCareProviderLname(res[0].care_provider_lname)
    };
    getData();
  }, []);

  const formSubmit = async (event) => {
    event.preventDefault();
    const response = await fetch("http://localhost:4000/patient_immunization", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        immuName,
        dose,
        patientId,
        careProviderId,
        date,
        comment,
        administrator,
      }),
    });
    if (response.status === 201) {
      alert(`Congrats! Submission submitted`);
    }
    navigate(`/patients/${patientId}/immune`);
  };

  return (
    <Container style={{ backgroundColor: "#B19CD9" }} className="my-5">
      <Form>
        <h3 style={{ padding: "10px", textAlign: "center" }} className="font">
          Add a new entry
        </h3>
        <FormGroup>
          <Label for="immuName">Immunization Name</Label>
          <Input
            type="text"
            name="immuName"
            id="immuName"
            placeholder="Immunization Name"
            onChange={(e) => setimmuName(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label for="dose">Dose</Label>
          <Input
            type="text"
            name="dose"
            id="dose"
            placeholder="Dose"
            onChange={(e) => setDose(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label for="dateCompleted">Date Completed</Label>
          <Input
            type="date"
            name="dateCompleted"
            id="dateCompleted"
            placeholder="Date Completed"
            onChange={(e) => setDate(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label for="comment">Comment</Label>
          <Input
            type="textarea"
            name="comment"
            id="comment"
            placeholder="Comment"
            onChange={(e) => setComment(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label for="administerBy">Administer By</Label>
          <Input
            type="text"
            name="administerBy"
            id="administerBy"
            placeholder="Administer By"
            onChange={(e) => setAdministrator(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label for="familyDoctor">Family Doctor</Label>
          <Input
            type="text"
            name="familyDoctor"
            id="familyDoctor"
            value = {careProviderFname + ' ' + careProviderMname + ' ' + careProviderLname}
            disabled />
        </FormGroup>
      </Form>
      <div style={{ padding: "10px", textAlign: "center" }}>
        <Button onClick={formSubmit}>Submit</Button>
      </div>
    </Container>
  );
};

export default ImmuneAdd;
