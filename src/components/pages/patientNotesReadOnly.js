import React from "react";
import { Button } from "reactstrap";

const PatientNotesReadOnly = ({user, handleUserEdit, handleEditUserDeleteButton}) => {
    return (
        // <div>
            <tr key={user.user_id}>
                <td style={{textAlign: "center"}}>{user.user_id }</td>
                <td>{user.user_first_name}</td>
                <td>{user.user_last_name}</td>
                <td>{user.email}</td>
                <td>
                    <div className="user-actionButtons-container">
                        <div className="user-editButton-container">
                            <Button size="sm" id="user-editButton"
                                className="editUserButton"
                                type="button"
                                style={{
                                  marginRight: "10%"
                                 }}  
                                onClick={(e) => handleUserEdit(e, user)}
                            >
                                Edit
                            </Button>                                                       
                        </div>     
                        <div className="1user-deleteButton-container">
                            <Button size="sm" id="1user-deleteButton" className="1deleteUserButton"
                                type="button"
                                onClick={(event) => handleEditUserDeleteButton(event, user.user_id)}
                            >
                                Delete
                            </Button>                                                       
                        </div>        
                    </div>                                                                                
                </td>


            </tr>


        // </div>

    )
}

export default PatientNotesReadOnly