import React, { useState, useEffect } from "react";
import { Form, FormGroup, Col, Row, Input, Label, Button, Container, CardBody, Card, CardText } from 'reactstrap';
import { useHistory, useLocation } from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from "axios";
import { Link } from 'react-router-dom';

function CareproviderDetail() {
  const [search, setSearch] = useState('');
  const [record, setRecord] = useState([]);

  const [user, setUser] = useState({
    fname: "",
    mname: "",
    lname: "",
    email: "",
    cpsonumber: "",
    initialsetup: "",
    careprovidertypeid: ""
  });




  //  Object Destructuring 
  const { fname, mname, lname, email, cpsonumber, initialsetup, careprovidertypeid } = user;
  const onInputChange = e => {
    setUser({ ...user, [e.target.name]: e.target.value });
  };


  // On Page load display all records 
  const loadCareproviderDetail = () => {
    fetch('http://localhost:4000/api/v1/careprovider')
      .then(function (response) {
        return response.json();
      })
      .then(function (myJson) {
        setRecord(myJson);
      });
  }

  useEffect(() => {
    loadCareproviderDetail();
  }, []);

  // Insert Careprovider Records 
  const submitCareproviderRecord = async (e) => {
    e.preventDefault();
    e.target.reset();
    await axios.post("http://localhost:4000/api/v1/careprovider", user);
    alert('Data Inserted');

    loadCareproviderDetail();
  };

  // Search Records here 

  const searchRecords = () => {
    alert(search)
    axios.get(`http://localhost:4000/api/v1/careprovider/searchRecord/${search}`)
      .then(response => {
        setRecord(response.data);
      });
  }

  // Delete careprovider Record

  const deleteRecord = (productId) => {
    axios.delete(`http://localhost:4000/api/v1/careprovider/${productId}`)
      .then((result) => {
        loadCareproviderDetail();
      })
      .catch(() => {
        alert('Error in the Code');
      });
  };

  return (
    <section>


      <nav class="navbar navbar-expand-lg navbar-light bg-dark">
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
          <ul class="navbar-nav">
            <li class="nav-item active ">
            </li>
            <li class="nav-item active">
              <a class="nav-link text-white" href="/admin">Edit Patients <span class="sr-only">(current)</span></a>
            </li>
          </ul>
        </div>
      </nav>

      <div class="container">
        <div class="row mt-3">
          <div class="col-sm-4">
            <div className="box p-3 mb-3 mt-5" style={{ border: "1px solid #d0d0d0" }}>
              <form onSubmit={submitCareproviderRecord}>
                <h5 className="mb-3 ">Insert Careprovider Records</h5>

                <div class="form-group">
                  <input type="text" class="form-control  mb-4" name="fname" value={fname} onChange={e => onInputChange(e)} placeholder="Enter name" required="" />
                </div>

                <div class="form-group">
                  <input type="text" class="form-control  mb-4" name="mname" value={mname} onChange={e => onInputChange(e)} placeholder="Enter middle name" required="" />
                </div>

                <div class="form-group">
                  <input type="text" class="form-control  mb-4" name="lname" value={lname} onChange={e => onInputChange(e)} placeholder="Enter lastname" required="" />
                </div>


                <div class="form-group">
                  <input type="text" class="form-control mb-4" name="email" value={email} onChange={e => onInputChange(e)} placeholder="Enter Email" required="" />
                </div>

                <div class="form-group">
                  <input type="text" class="form-control mb-4" name="cpsonumber" value={cpsonumber} onChange={e => onInputChange(e)} placeholder="Cpso number" required="" />
                </div>

                <div class="form-group">
                  <input type="text" class="form-control mb-4" name="initialsetup" value={initialsetup} onChange={e => onInputChange(e)} placeholder="Enter Inital setup code" required="" />
                </div>

                <div class="form-group">
                <input type="text" class="form-control mb-4" name="careprovidertypeid" value={careprovidertypeid} onChange={e => onInputChange(e)} placeholder="Enter Care Provider id type" required="" />
              </div>

                <button type="submit" class="btn btn-primary btn-block mt-4">Insert Record</button>
              </form>
            </div>
          </div>
          <div class="col-sm-8">
            <h4 class="text-center  ml-4 mt-4  mb-5">View Care Providers</h4>
            <div class="input-group mb-4 mt-3">
              <div class="form-outline">
                <input type="text" id="form1" onChange={(e) => setSearch(e.target.value)} class="form-control" placeholder="Search CareProvider Here" style={{ backgroundColor: "#ececec" }} />
              </div>
              <button type="button" onClick={searchRecords} class="btn btn-success">
                <i class="fa fa-search" aria-hidden="true"></i>
              </button>
            </div>
            <table class="table table-hover  table-striped table-bordered ml-4 ">
              <thead>
                <tr>
                  <th>First Name</th>
                  <th>Middle Name</th>
                  <th>Last Name</th>
                  <th>Email</th>
                  <th>Cpso Number</th>
                  <th>Initial Setup</th>
                  <th>CareProvider Type ID</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>

                {record.map((name) =>
                  <tr>
                    <td>{name.first_name}</td>
                    <td>{name.middle_name}</td>
                    <td>{name.last_name}</td>
                    <td>{name.email}</td>
                    <td>{name.cpso_number}</td>
                    <td>{name.initial_setup}</td>
                    <td>{name.care_provider_type_id}</td>
                    <td>

                      <a className="text-danger mr-2"
                        onClick={() => {
                          const confirmBox = window.confirm(
                            "Do you really want to delete " + name.first_name
                          )
                          if (confirmBox === true) {
                            deleteRecord(name.id)
                          }
                        }}> <i class="far fa-trash-alt" style={{ fontSize: "18px", marginRight: "5px" }}></i> </a>

                      <Link class=" mr-2" to={`/EditCareprovider ${name.id}`}>
                        <i class="fa fa-edit" aria-hidden="true"></i>
                      </Link>
                    </td>
                  </tr>
                )}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </section>
  )
}

export default CareproviderDetail;