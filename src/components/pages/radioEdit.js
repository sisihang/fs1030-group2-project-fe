import React, { useState, useEffect } from "react";
import { useParams } from "react-router";
import { useNavigate } from "react-router-dom";
import {
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  FormText,
  Container,
  Row,
} from "reactstrap";

const RadioEdit = () => {

  const { id } = useParams();
  const patientId = id;
  const navigate = useNavigate();
  const [radiology, setRadiology] = useState([]);
  const [form, setForm] = useState({ display: "none" });
  const [radio, setRadio] = useState({ radio_image: "", image_type: "", radiographer: "", radio_date: "", clinic_name: "", comments: "", });
  const [patients, setPatients] = useState([]);

  useEffect(() => {
    const getData = async () => {
      const patientResponse = await fetch(
        `http://localhost:4000/api/patients/${id}`
      );
      const patientData = await patientResponse.json();
      setPatients(patientData);
    };
    getData();
  }, [id]);

  useEffect(() => {
    async function fetchData() {
      const res = await fetch(`http://localhost:4000/radiology/${id}`);

      res
        .json()
        .then((res) => setRadiology(res))
        .catch((err) => console.log(err));


    }
    fetchData();
  }, [id]);

  const handleEdit = (event, rad) => {
    event.preventDefault();
    setForm({ display: "block" });
    setRadio(rad);
    console.log(rad);
  };



  const handleChange = (event) => {
    setRadio((prevState) => ({
      ...prevState,
      [event.target.name]: event.target.value,
    }));
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    // console.log(id);
    fetch(`http://localhost:4000/radiology/${id}`, {
      method: "put",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },

      //make sure to serialize your JSON body
      body: JSON.stringify(radio),
    }).then((response) => response.json());
    navigate(`/patients/${patientId}/radio`);
  };

  return (

    <Container style={{ backgroundColor: "#7eb26d" }}>

      <Row style={{ padding: '30px' }}>
        {patients.map((patient) => (
          <h5>
            Edit Radiology - Name:{" "}
            {patient.first_name +
              " " +
              patient.middle_name +
              " " +
              patient.last_name + " "}
            <br /> Patient ID: {" "}
            {patient.patient_id}
          </h5>
        ))}
      </Row>
      <Row className="m-3">
        {radiology.map((rad) => (
          <div key={rad.id}>
            <p>Radiograph ID: &nbsp;{rad.radiograph_id}
            &nbsp;&nbsp;&nbsp;&nbsp;
            <img src={rad.radio_image} alt="" width="100" height="100" />
            <button className="mx-5 px-5 " onClick={(e) => handleEdit(e, rad)}>Edit</button></p>
            <hr />
          </div>
        ))}
      </Row>
      <Form className="m-3" onSubmit={(e) => handleSubmit(e)} style={form} >
         <FormGroup>
            <Label >Image:</Label>
            <Input
              type="text"
              name="radio_image"
              value={radio.radio_image}
              onChange={handleEdit}
            />
          </FormGroup>
          <FormGroup>
          <Label>Image Type:</Label>
          <Input
            type="text"
            name="image_type"
            value={radio.image_type}
            onChange={handleChange}
          />
        </FormGroup>
        <FormGroup>
          <Label>Radiographer:</Label>
          <Input
            type="text"
            name="radiographer"
            value={radio.radiographer}
            onChange={handleChange}
          />
        </FormGroup>
        <FormGroup>
          <Label>Date:</Label>
          <Input
            type="date"
            name="radio_date"
            value={radio.radio_date}
            onChange={handleChange}
          />
        </FormGroup>

        <FormGroup>
          <Label for="clinicName">Clinic Name:</Label>
          <Input
            type="text"
            name="clinic_name"
            value={radio.clinic_name}
            onChange={handleChange}
          />
        </FormGroup>
        <FormGroup>
          <Label for="comments">Comments:</Label>
          <Input
            type="textarea"
            name="comments"
            value={radio.comments}
            onChange={handleChange}
          />
        </FormGroup>

        <Button type="submit" value="submit">Submit</Button>
      </Form >
    </Container>
  );
};




export default RadioEdit;