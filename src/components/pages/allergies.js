import { useEffect, useState, React } from "react";
import { useParams } from "react-router";
import {
  Container,
  Row,
  Col,
  Button,
  CardBody,
  CardText,
  Card,
  Table,
} from "reactstrap";
import Navigation from "../shared/navigation";
import { useNavigate } from "react-router-dom";

const Allergies = () => {
  const { id } = useParams();
  const [allergies, setAllergies] = useState([]);
  const [patients, setPatients] = useState([]);
  const navigate = useNavigate();

  const redirectAdd = (event) => {
    event.preventDefault();
    navigate(`/patients/${id}/allergies/add`);
  };

  const handleDelete = (event, allergy) => {
    event.preventDefault();
    fetch(`http://localhost:4000/patient_allergy/${allergy.allergy_id}`, {
      method: "delete",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    }).then((response) => response.json());
    window.location.reload(false);
  };

  useEffect(() => {
    const getData = async () => {
      const allergyResponse = await fetch(
        `http://localhost:4000/patient_allergy/${id}`
      );
      const allergyData = await allergyResponse.json();
      setAllergies(allergyData);

      const patientResponse = await fetch(
        `http://localhost:4000/api/patients/${id}`
      );
      const patientData = await patientResponse.json();
      setPatients(patientData);
    };
    getData();
  }, []);

  return (
    <Container style={{ backgroundColor: "#B19CD9" }} className="my-5">
      <Navigation patientID={id} />
      <Row style={{ padding: "10px" }}>
        {patients.map((patient) => (
          <h3>
            Patient Name:{" "}
            {patient.first_name +
              " " +
              patient.middle_name +
              " " +
              patient.last_name}
          </h3>
        ))}
      </Row>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Allergy Name</th>
            <th>Serverity</th>
            <th>Comments</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {allergies.length > 0 &&
            allergies.map((allergy) => (
              <tr>
                <td>{allergy.allergy_name}</td>
                <td>{allergy.serverity}</td>
                <td>{allergy.comments}</td>
                <td>
                  <Button
                    onClick={(e) => {
                      handleDelete(e, allergy);
                    }}
                    size="sm"
                  >
                    Delete
                  </Button>
                </td>
              </tr>
            ))}
        </tbody>
      </Table>
      <div className="text-center">
        <Button onClick={redirectAdd}>Add New</Button>
      </div>
    </Container>
  );
};

export default Allergies;
