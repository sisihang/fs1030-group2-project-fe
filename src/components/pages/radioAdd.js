import { useEffect, useState, React } from "react";
import { useParams } from "react-router";
import {
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  FormText,
  Container,
  Row,
} from "reactstrap";
import { useNavigate, Link } from "react-router-dom";

const RadioAdd = () => {
  const { id } = useParams();
  const patientId = id;
  const [imageType, setImageType] = useState("");
  const [radioImage, setImage] = useState("");
  const [radiographer, setRadiographer] = useState("");
  const [careProviderId, setCareProviderId] = useState("");
  const [radioDate, setDate] = useState("");
  const [clinicName, setClinicName] = useState("");
  const [comments, setComments] = useState("");
  const [patients, setPatients] = useState([]);


  const navigate = useNavigate();

  useEffect(() => {
    const getData = async () => {
      const patientRes = await fetch(
        `http://localhost:4000/api/patients/${patientId}`
      );

      const res = await patientRes.json();
      setCareProviderId(res[0].family_doctor_code);

    };
    getData();
  }, []);

  useEffect(() => {
    const getData = async () => {
        const patientResponse = await fetch(
            `http://localhost:4000/api/patients/${id}`
          );
          const patientData = await patientResponse.json();
          setPatients(patientData);
    };
    getData();
  }, []);

  const formSubmit = async (event) => {
    event.preventDefault();
    const response = await fetch("http://localhost:4000/radiology", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        imageType,
        radioImage,
        patientId,
        radiographer,
        careProviderId,
        radioDate,
        clinicName,
        comments,
      }),
    });
    if (response.status === 201) {
      alert(`Congrats! Submission submitted`);
    }
    navigate(`/patients/${patientId}/radio`);
  };

  return (
    <Container style={{ backgroundColor: "#7eb26d" }} className="my-5">
      <Form>
      <Row style={{ padding: '30px' }}>
                {patients.map((patient) => (
                    <h5>
                        Radiology - Name:{" "}
                        {patient.first_name +
                            " " +
                            patient.middle_name +
                            " " +
                            patient.last_name + " "}
                           <br /> Patient ID: {" "}
                            {patient.patient_id}
                    </h5>
                    ))}
            </Row>
        <FormGroup>
          <Label for="image">Image</Label>
          <Input
            type="text"
            name="image"
            id="image"
            placeholder="Place Image URL here"
            onChange={(e) => setImage(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label for="imageType">Image Type</Label>
          <Input
            type="text"
            name="imageType"
            id="imageType"
            placeholder="Image Type"
            onChange={(e) => setImageType(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label for="radiographer">Radiographer</Label>
          <Input
            type="text"
            name="radiographer"
            id="radiographer"
            placeholder="Radiographer Name"
            onChange={(e) => setRadiographer(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label for="careProviderId">Care Provider</Label>
          <Input
            type="text"
            name="careProviderId"
            id="careProviderId"
            value = {careProviderId}
            disabled />
        </FormGroup>
        <FormGroup>
          <Label for="date">Date</Label>
          <Input
            type="date"
            name="date"
            id="date"
            placeholder="Date"
            onChange={(e) => setDate(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label for="clinicName">Clinic Name</Label>
          <Input
            type="text"
            name="clinicName"
            id="clinicName"
            placeholder="Clinic Name"
            onChange={(e) => setClinicName(e.target.value)}
           /> 
        </FormGroup>
        <FormGroup>
          <Label for="comments">Comment</Label>
          <Input
            type="textarea"
            name="comments"
            id="comments"
            placeholder="Comments"
            onChange={(e) => setComments(e.target.value)}
          />
        </FormGroup>
       
      </Form>
      <div style={{ padding: "10px", textAlign: "center" }}>
        <Button onClick={formSubmit}>Submit</Button>
       
      </div>
    </Container>
  );
};

export default RadioAdd;
