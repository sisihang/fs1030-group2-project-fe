import React, { useState, useEffect, Fragment } from "react";
import { NavLink as RouteLink } from "react-router-dom";
import { useParams } from "react-router";
// import { useHistory } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { Container, Form, FormGroup, Label, Input, Row, Col, Table, Button } from 'reactstrap'
import "../styles/patientView.css"
import PatientContactDetailsReadOnly from "./patientContactDetailsReadOnly";
import PatientContactDetailsEdit from "./patientContactDetailsEdit";
import PatientDemographicsReadOnly from "./patientDemographicsReadOnly";
import PatientDemographicsEdit from "./patientDemographicsEdit";
import PatientEmergencyReadOnly from "./patientEmergencyReadOnly";
import PatientEmergencyEdit from "./patientEmergencyEdit"
import Navigation  from '../shared/navigation'


const PatientView = () => {

    // let history = useHistory();
    const navigate = useNavigate();

    const { id } = useParams();
    const currentPatientID = id

    const [patients, setPatients] = useState([]);
    const [selectedContactID, setSelectedContactID] = useState([]);
 
    const [editContactDetailsMode, setEditContactDetailsMode] = useState(false);
    const [editDemoMode, setEditDemoMode] = useState(false);
    const [editEmergencyMode, setEditEmergencyMode] = useState(false);
   
    const [raceDropDownValues, setRaceDropDownValues] = useState([]);

    const [provinceLookupValue, setProvinceLookupValue] = useState([])
    const [selectedProvince, setSelectedProvince] = useState({
          province_code: "",
          province_name: ""
    })

    const [updatedFields, setUpdatedFields] =useState([])


    useEffect(() => {
      async function fetchData() {
        const res = await fetch(`http://localhost:4000/api/patients/${id}`);
        res
          .json()
          .then((res) => setPatients(res))
          .catch((err) => console.log(err));
       }


      fetchData();
      setSelectedProvince(patients)
     
    }, [id]);
        

    useEffect(() => {
      async function fetchData() {
        const res = await fetch("http://localhost:4000/api/province");
        res
          .json()
          .then((res) => setProvinceLookupValue(res))
          .catch((err) => console.log(err));
      }
        fetchData();
    }, []);
  

  const handleProvinceChange = (e) => {
    setSelectedProvince({province_name: e.target.value})
  }    

    // State to handle the edit Contact Details Form
    const [contactDetailsEditFormData, setContactDetailsEditFormData] = useState({ 
      first_name: "",
      middle_name: "",
      last_name: "",
      address_line1: "",
      address_line2: "",
      city: "",
      province_code: "",
      postal_code: "",
      home_phone: "",
      cell_phone: "",
      email: "",
      family_doctor_code: ""
    });





  // State to handle the edit Demographics Form
  const [demoEditFormData, setdemoEditFormData] = useState({ 
    employer_name: "",
    work_phone: "",
    occupation: "",
    dob: "",
    patient_id: currentPatientID,
    gender_code: "",
    pref_language_id: "",
    race_id: "",
    marital_status_code: ""
  });    

  // State to handle the Emergency EDIT Form
  const [emergencyEditFormData, setEmergencyEditFormData] = useState({ 
    contact_fname: "",
    contact_lname: "",
    contact_phone: "",
    relationship: "",
    patient_id: ""
  });  
 
     // Get data from the current state then store in the Content Details Form state
     const contactDetailsEditFormValues = {
      first_name: patients.first_name,
      middle_name: patients.middle_name,
      last_name: patients.last_name,
      address_line1: patients.address_line1,
      address_line2: patients.address_line2,
      city: patients.city,
      province_code: patients.province_code,
      postal_code: patients.postal_code,
      home_phone: patients.home_phone,
      cell_phone: patients.cell_phone,
      email: patients.email,
      family_doctor_code: patients.family_doctor_code
    };

     // Get data from the current state then store in the Demographics Form state
     const demographicsFormValues = {
      dob: patients.dob,
      gender_code: patients.gender_code,
      marital_status_code: patients.marital_status_code,
      race_id: patients.race_id,
      pref_language_id: patients.pref_language_id,
      employer_name: patients.employer_name,
      work_phone: patients.work_phone,
      occupation: patients.occupation
    };
    
    

    const handleContactDetailsEditCancelClick = () => {
      setEditContactDetailsMode(false)
    }

    
    const handleDemoEditCancelClick = () => {
      setEditDemoMode(false)
    }
  
    
    const handleEmergencyEditCancelClick = () => {
      setEditEmergencyMode(false)
    }
 

  const handlePatientViewEditClick = (event, patient) => {
    event.preventDefault();
    // setEditPatientID(patient.patient_id)
    setEditContactDetailsMode(true)
    setContactDetailsEditFormData(patient)
  }

  const handleDemoViewEditClick = (event, patient) => {
    event.preventDefault();
    // setEditPatientID(patient.patient_id)
    setEditDemoMode(true)
    setdemoEditFormData(patient)
  }  

  const handleEmergencyViewEditClick = (event, emergItem) => {
    event.preventDefault();
    // setEditPatientID(patient.patient_id)
    setEditEmergencyMode(true)
    setSelectedContactID(emergItem)
    
  }   

   // Handler for changes in Contact Details Edit form
   const handleContactDetailsEditFormChange = (event) => {
        event.preventDefault();
        const fieldName = event.target.getAttribute("name");
        const fieldValue = event.target.value;
        const newContactDetailsEditFormData = { ...contactDetailsEditFormData };
        newContactDetailsEditFormData[fieldName] = fieldValue;
        setContactDetailsEditFormData(newContactDetailsEditFormData);

        setUpdatedFields([...updatedFields,fieldName])
    }

   // Handler for changes in Demographics Edit form
   const handleDemoEditFormChange = (event) => {
    event.preventDefault();
    const fieldName = event.target.getAttribute("name");
    const fieldValue = event.target.value;
    const newDemoEditFormData = { ...demoEditFormData };
    newDemoEditFormData[fieldName] = fieldValue;
    setdemoEditFormData(newDemoEditFormData);  
}    
//dito
  // Handler for changes in Emergency Edit form
    const handleEmergencyEditFormChange = (event) => {
    event.preventDefault();
    const fieldName = event.target.getAttribute("name");
    const fieldValue = event.target.value;
    const newEmergencyEditFormData = { ...emergencyEditFormData };
    newEmergencyEditFormData[fieldName] = fieldValue;
    setEmergencyEditFormData(newEmergencyEditFormData);  
}  

   // Handles the SUBMIT button in Contact Details EDIT form
    const handleContactDetailsEditFormSubmit = (event) => {
      event.preventDefault();
      const editedPatient = {
          first_name: contactDetailsEditFormData.first_name,
          middle_name: contactDetailsEditFormData.middle_name,
          last_name: contactDetailsEditFormData.last_name,
          address_line1: contactDetailsEditFormData.address_line1,
          address_line2: contactDetailsEditFormData.address_line2,
          city: contactDetailsEditFormData.city,
          province_code: selectedProvince.province_code,
          postal_code: contactDetailsEditFormData.postal_code,
          home_phone: contactDetailsEditFormData.home_phone,
          cell_phone: contactDetailsEditFormData.cell_phone,
          email: contactDetailsEditFormData.email    
      }      
      fetch(`http://localhost:4000/api/patients/${id}`, {
        method: "put",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        //make sure to serialize your JSON body
        // body: JSON.stringify(editedPatient),
        body: JSON.stringify(contactDetailsEditFormData),
      }).then((response) => response.json());
      // navigate('/');

      //updated revision history
      const uniqueUpdatedFields = [...new Set(updatedFields)];
      const stringUpdatedFields  = uniqueUpdatedFields.toString();
      const dateUpdtaed = new Date();
      const patientId = patients[0].patient_id
      const careProviderId = patients[0].family_doctor_code

      fetch(`http://localhost:4000/revision_history`, {
        method: "post",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        //make sure to serialize your JSON body
        body: JSON.stringify({
          dateUpdtaed,
          stringUpdatedFields,
          patientId,
          careProviderId
        }),
      }).then((response) => response.json());


      alert('Record Saved!')
      setEditContactDetailsMode(false)
      // history.push('/');
    }
    

    // dito
     // Handles the SUBMIT button in EMERGENCY CONTACT EDIT form
    const handleEmergencyEditFormSubmit = (event) => {
      event.preventDefault();
      const editedContact = {
          contact_fname: emergencyEditFormData.first_name,
          contact_lname: emergencyEditFormData.middle_name,
          contact_phone: emergencyEditFormData.last_name,
          relationship: emergencyEditFormData.address_line1,
          patient_id: emergencyEditFormData.address_line2
      }      
      fetch(`http://localhost:4000/api/patients/${id}`, {
        method: "put",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        //make sure to serialize your JSON body
        // body: JSON.stringify(editedPatient),
        body: JSON.stringify(contactDetailsEditFormData),
      }).then((response) => response.json());
      // navigate('/');

      //updated revision history
      const uniqueUpdatedFields = [...new Set(updatedFields)];
      const stringUpdatedFields  = uniqueUpdatedFields.toString();
      const dateUpdtaed = new Date();
      const patientId = patients[0].patient_id
      const careProviderId = patients[0].family_doctor_code

      fetch(`http://localhost:4000/revision_history`, {
        method: "post",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        //make sure to serialize your JSON body
        body: JSON.stringify({
          dateUpdtaed,
          stringUpdatedFields,
          patientId,
          careProviderId
        }),
      }).then((response) => response.json());


      alert('Record Saved!')
      setEditContactDetailsMode(false)
      // history.push('/');
    }
    

    // Handles the SUBMIT button on DEMOGRAPHICS EDIT form
    const handleDemoEditFormSubmit = (event) => {
      event.preventDefault();
      const editedPatientDemo = {
          employer_name: demoEditFormData.employer_name,
          work_phone: demoEditFormData.work_phone,
          occupation: demoEditFormData.occupation,   
          dob: "1975-01-01",
          patient_id: 5,
          gender_code: demoEditFormData.gender_code,
          pref_language_id: demoEditFormData.pref_language_id,
          race_id: demoEditFormData.race_id,
          marital_status_code: demoEditFormData.marital_status_code,      
      }      
      fetch(`http://localhost:4000/api/patient_demographics/5`, {
        method: "put",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        //make sure to serialize your JSON body
        body: JSON.stringify(editedPatientDemo),
      }).then((response) => response.json());
      alert('Record Saved!')
      setEditDemoMode(false)
      // history.push('/');
      // navigate('/patients/${id}')
    }





  // Function to format DOB
  // const formatDOB = (dob) => {
  // const formattedDOB = new Date(dob).toLocaleDateString();
  // return formattedDOB;
  // }
  
  return (

    <Container>
        <Navigation patientID={id} />  
        <section className="patient-listing-container">
          {patients.map( (patient) => (
           
             <div key={patient.patient_id}> 
             {/* ==== PatientContact Details ==== */}

             <Row>
              <div className="xxviewPatientInfo-header-container">
                   <div className="xxviewPatientInfoTitle-header-container">
                      <p>Currently serving patient:     <b>{patient.first_name + " " + patient.middle_name + " " + patient.last_name}</b></p>
                      <label id="contactDetailsReadOnly-label" className="xxcontactDetailsReadOnly-label">Health Card Number:   <b>{patient.healthcard_no + "-" + patient.healthcard_ver}</b></label> <br></br>  
                      <label className="xxviewPatient-patientIdlabel">Patient ID:   <b>{patient.patient_id}</b></label> <br></br> 
                      {/* <label className="viewPatient-patientNamelabel">Patient Name:   {patient.patient_fname + " " + patient.patient_mname + " " + patient.patient_lname}</label>      */}
                                                                
                   </div>
                   {/* <div className="xxviewPatientInfoTitle2-header-container">   
                      <label className="xxviewPatient-doctorNamelabel">Hello {patient.doctor_fname + " " + patient.doctor_mname + " " + patient.doctor_lname} !</label>                              
                  </div> */}
              </div>
            </Row>
                         
                <Form onSubmit={handleContactDetailsEditFormSubmit}>
                    <Fragment>
                        {editContactDetailsMode === true ? (
                            <PatientContactDetailsEdit 
                              patient={patient}
                              handleProvinceChange={handleProvinceChange}
                              selectedProvince={selectedProvince}
                              provinceLookupValue={provinceLookupValue}
                              provinceLookupValue={provinceLookupValue}
                              contactDetailsEditFormData={contactDetailsEditFormData}
                              handleContactDetailsEditFormChange={handleContactDetailsEditFormChange}
                              handleContactDetailsEditFormSubmit={handleContactDetailsEditFormSubmit}
                              handleContactDetailsEditCancelClick={handleContactDetailsEditCancelClick}
                            />
                        ) : (
                            <PatientContactDetailsReadOnly 
                              patient={patient}
                              handlePatientViewEditClick={handlePatientViewEditClick}
                              
                            />
                        )}
                    </Fragment>
                </Form>
                                
                {/*  === PATIENT DEMOGRAPHICS ===*/}
                <Form onSubmit={handleDemoEditFormSubmit}>
                    <Fragment>
                        {editDemoMode === true ? (
                            <PatientDemographicsEdit 
                            patient={patient}
                            demoEditFormData={demoEditFormData}
                            handleDemoEditFormChange={handleDemoEditFormChange}
                            handleDemoEditCancelClick={handleDemoEditCancelClick}
                            />
                        ) : (
                            <PatientDemographicsReadOnly 
                              // emergencyList={emergencyList}
                              patient={patient}
                              handleDemoViewEditClick={handleDemoViewEditClick}
                              
                            />
                        )}
                    </Fragment>
                </Form>

                {/*  === PATIENT EMERGENCY CONTACTS ===*/}
                <Form onSubmit={handleEmergencyEditFormSubmit}>
                    <Fragment>
                        {editEmergencyMode === true ? (
                            <PatientEmergencyEdit
                           
                            handleDemoEditCancelClick={handleDemoEditCancelClick}
                            selectedContactID={selectedContactID}
                            currentPatientID={currentPatientID}
                            handleEmergencyEditCancelClick={handleEmergencyEditCancelClick}
                            handleEmergencyEditFormChange={handleEmergencyEditFormChange}
                            // patient={patient}
                            // demoEditFormData={demoEditFormData}
                            // handleDemoEditFormChange={handleDemoEditFormChange}

                            />
                        ) : ( // dito
                            <PatientEmergencyReadOnly 
                              // emergencyList={emergencyList}
                              // searchNote={searchNote}
                              // patient={patient}
                              currentPatientID={currentPatientID}
                              selectedContactID = {selectedContactID}
                              editEmergencyMode={editEmergencyMode}
                              handleEmergencyViewEditClick={handleEmergencyViewEditClick}
                              
                            />
                        )}
                    </Fragment>
                </Form>                
                
                                
            </div>              
          ))}
        </section>   
    </Container>
  );
};

export default PatientView;
