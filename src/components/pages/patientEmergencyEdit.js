import React, { useState, useEffect } from "react";
import { useParams } from "react-router";
import { NavLink as RouteLink } from "react-router-dom";
import { Container, Form, FormGroup, Label, Input, Row, Col, Table, Button } from 'reactstrap'

// import Select from 'react-select';
// import "../styles/patientDemographicsEdit.css"
// import ProvinceLookup from "./lookup/ProvinceLookup-toremove";
import "../styles/patientEmergencyEdit.css"


const PatientEmergencyEdit = ({ currentPatientID, selectedContactID, editEmergencyMode, handleEmergencyEditCancelClick, handleEmergencyEditFormChange }) => {

    const [currentContact, setCurrentContact] = useState([])
    
    useEffect(() => {
        const fetchData = async () => {
          const contactRes = await fetch(
            `http://localhost:4000/api/patient_emergency/${currentPatientID}/${selectedContactID}`
          );
          const res = await contactRes.json();

            setCurrentContact(res[0]);
        };
        fetchData();
      }, [currentPatientID, selectedContactID]);
       


    return (
        <div className="editDemo-maincontainer">
            <Row>
                <div className="editDemo-cardTitle-container">
                  <Col lg="6" sm="6" xs="12">
                    <div className="editDemo-header-container">
                         Emergency Contact
                    </div>    
                  </Col>
                  <Col lg="6" sm="6" xs="12">              

                    <div className="editEmergency-button-container">
                            <Button size="sm" 
                                id="editEmergency-save-button" 
                                className="editEmergency-save-button"  
                                // style={{disabled: editEmergencyMode ? true: false}}                   
                                // onClick={(e) => formatDateToYYYMMDD(patient.dob)}
                           >
                            Save
                          </Button>

                          <Button size="sm" className="editEmergency-cancel-button"
                              onClick={handleEmergencyEditCancelClick}
                            >Cancel
                         </Button>                                                                           
                    </div>
                  </Col>
                  </div>
                  <br></br> <br></br>
              </Row>

              <Row>
                <div className="editEmergency-container">
                    <Col lg="6" sm="6" xs="12">
                        <div id="editEmergency-mainInfo-container"> 
                            <label id="editEmergency-label" className="editEmergency-label">First Name   :</label> 
                            <input id="editEmergency-textbox" className="editEmergency-textbox"
                                type="text"
                                name="contact_fname"
                                value={currentContact.contact_fname}
                                onChange={handleEmergencyEditFormChange}
                            ></input> <br></br>
                        </div>

                        <div id="editEmergency-mainInfo-container"> 
                            <label id="editEmergency-label" className="editEmergency-label">Last Name   :</label> 
                            <input id="editEmergency-textbox" className="editEmergency-textbox"
                                type="text"
                                name="contact_lname"
                                value={currentContact.contact_lname}
                                onChange={handleEmergencyEditFormChange}
                            ></input> <br></br>
                        </div>                        

                    </Col>

                    <Col lg="6" sm="6" xs="12">
                        <div id="editEmergency-workInfo-container">
                            <label id="editEmergency-label" className="editEmergency-label">Phone   :</label> 
                            <input id="editEmergency-textbox" className="editEmergency-textbox"
                                type="text"
                                name="contact_phone"
                                value={currentContact.contact_phone}
                                defaultValue={currentContact.contact_phone}
                                onChange={handleEmergencyEditFormChange}
                            ></input> <br></br>   

                            <label id="editEmergency-label" className="editEmergency-label">Relationship   :</label> 
                            <input id="editEmergency-textbox" className="editEmergency-textbox"
                                type="text"
                                name="relationship"
                                value={currentContact.relationship}
                                defaultValue={currentContact.relationship}
                                onChange={handleEmergencyEditFormChange}
                            ></input> <br></br>                                                                                                                                             
                
                        </div>
                    </Col>                       

              
                </div>  
                 
              </Row> 
        </div>
    )
}

export default PatientEmergencyEdit
