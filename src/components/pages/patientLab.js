import React, { useEffect, useState } from 'react'
import { Container, Form, FormGroup, Label, Input, Row, Col, Table, Button } from 'reactstrap'
// import { useHistory } from "react-router-dom";
import { useNavigate } from 'react-router-dom';
import { useParams } from "react-router";
// import { RiLogoutBoxRLine } from "react-icons/ri"
// import parseJwt from '../../helpers/authHelper'
// import "../styles/patientsLab.css"
import "../styles/patientLab.css"
// import PatientView from './PatientView';
// import PatientEditContactDetails from './PatientEditContactDetails';
import Navigation from "../shared/navigation";
import PatientLabReadOnly from './patientLabReadOnly';
import PatientLabEdit from './patientLabEdit'


const PatientLab = () => {

    const { id } = useParams();
    const patientID = id;

    const [labList, setLabList] = useState([]);
    const [labAddFormData, setlabAddFormData] = useState({
        lab_date: '', 
        laboratory: '', 
        lab_code: '',
        technician : '', 
        result: '', 
        comments: '', 
        patient_id: id,
        lab_order_by: ''
    })

    const [searchLab, setsearchLab] = useState([])

    const navigate = useNavigate();

    // Function to format date
    const formatDateToMMDDYYYY = (dateFieldToFormat) => {
        const formattedDate = new Date(dateFieldToFormat).toLocaleDateString();
        return formattedDate;
    } 

    const redirectToEditForm = (event) => {
        event.preventDefault();
        navigate(`/patients/${id}/lab/edit`);
    };

    const redirectToAddForm = (event) => {
        event.preventDefault();
        navigate(`/patients/${id}/lab/add`);
    };    

    useEffect(() => {
        const getData = async () => {
          const Labres = await fetch(
            `http://localhost:4000/api/patient_laboratory/${id}`
          );
          const labData = await Labres.json();
          setLabList(labData);
    

        };
        getData();
      }, [id]);



    const handleLabSearchChange = event => {
        setsearchLab(event.target.value);
    };    


   // Handles changes in Add form 
    const handleAddLabForm_FieldValueChange = (event) => {
        event.preventDefault();
        const labFieldName = event.target.getAttribute("name");
        const labFieldValue = event.target.value;

        const newlabFormData = { ...labAddFormData };
        newlabFormData[labFieldName] = labFieldValue
        setlabAddFormData(newlabFormData)
    }

    // Handles the edit button in the patient lab list - direct to Lab Edit Form
    const labEditFormRoute = (event, labItem) => {
        event.preventDefault();
        navigate(`/patients/${id}/lab/edit/${labItem.lab_id}`);        
      }


    // Handles the SUBMIT button in ADD FORM
    const handleAddLabForm_SubmitButton = () => {
        console.log("submitted")
        fetch(`http://localhost:4000/api/patient_laboratory/${id}`, {
            method: "POST",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
            },
            //make sure to serialize your JSON body
            body: JSON.stringify(labAddFormData),
          }).then((response) => response.json());
          navigate(`/patients/${id}/lab`);
          
    };    

   

    // Handles DELETE button in LAB LIST
    // const handleLabList_DeleteButton = (event, labItem) => {
    //     event.preventDefault();
    
    //     fetch(`/api/patient_laboratory/${labItem.lab_id}`, {
    //       method: "delete",
    //       headers: {
    //         Accept: "application/json",
    //         "Content-Type": "application/json",
    //       },
    //     }).then((response) => response.json());
    
    //     navigate(`/patients/${labItem.patient_id}/lab`);
        
    //   };


    const handleLabList_DeleteButton = (event, labIDToDelete) => {
        event.preventDefault();

      

        // var deleteConfirm = comfirm("Please confirm record deletion.")
        if (window.confirm("This will permanently remove this lab item.")) {

            fetch(`http://localhost:4000/api/patient_laboratory/${labIDToDelete}`, {
                method: "delete",
                headers: {
                  Accept: "application/json",
                  "Content-Type": "application/json",
                },
              }).then((response) => response.json());
    
            const newUsers = [...labList]
            const index = newUsers.findIndex((lab, index) => lab.lab_id === labIDToDelete)
    
            newUsers.splice(index, 1)
         
            setLabList(newUsers)
            // getData_AfterAdd()
        }
    }    

 

    return (
        <Container>
            {/*  To Search  */}
          <main className="lab-main-container">
                <Row>
                    <Form className="my-1">
                        <FormGroup row>
                            <Col xs={12} sm={4} md={3} lg={2}>     
                                <Label 
                                    style={{fontSize: ".8rem"}}>
                                     Laboratory Type :</Label>
                            </Col>
                            <Col xs={12} sm={6} md={5} lg={4}>  
                                    <Input 
                                        type="text" 
                                        name="search-labType" 
                                        id="search-labType" 
                                        placeholder="Enter type of Laboraory service to search" 
                                        style={{fontSize: ".8rem"}} 
                                        maxlength="10"
                                        required
                                        value={searchLab}
                                        onChange={handleLabSearchChange}
                                    />
                            </Col>
                            <Col xs={12} sm={2} md={4} lg={6}>  
                                <Button size="sm" 
                                        style={{fontSize: ".8rem", float: "right"}}
                                        onClick = {redirectToAddForm}
                                >
                                    Add
                                </Button>
                            </Col>
                        </FormGroup>  
     

                    </Form>

                    {/* <div className="addLab-button-container-top">
                        <Button size="sm" className="addLab-add-button-top"
                        // onClick={(event) => handlePatientViewEditClick(event, patient)}
                        >Add
                        </Button>                        
                     </div> */}
                 
                </Row>                           
               </main>

                <section className="lab-listing-container">
                    <Row>
                        <Table striped hover bordered responsive size="sm" >
                            <thead>
                                <tr>
                                    <th className="column-lab-id">ID</th>
                                    <th className="column-lab-date">Lab Date</th>   
                                    <th className="column-lab-desc">Lab Type</th>
                                    <th className="column-lab-result">Result</th>
                                    <th className="column-lab-comments">Comments</th>  
                                    <th className="column-lab-laboratory">Laboratory Name</th>                                     
                                    <th className="column-lab-orderby">Technician</th>
                                    <th className="column-lab-careprovider">Care Provider</th>
                                    <th className="column-lab-action">Action</th>

                                </tr>
                            </thead>
                            <tbody>
                                {labList.length === 0 &&
                                    <tr><td colSpan="4" className="text-center"><i>No listings found</i></td></tr>
                                }
                                {labList.length > 0 &&
                                    labList.filter((labItem) => {
                                        if (searchLab === "") {
                                            return labItem
                                        } else if(labItem.lab_desc.includes(searchLab)) {
                                            return labItem

                                        }
                                    }).map(labItem => 
                                        <tr key={labItem.lab_id}>
                                            <td style = {{ textAlign: "center"}} >{labItem.lab_id}</td>
                                            <td>{formatDateToMMDDYYYY(labItem.lab_date)}</td>
                                            <td>{labItem.lab_desc}</td>
                                            <td>{labItem.result}</td>
                                            <td>{labItem.comments}</td> 
                                            <td>{labItem.laboratory}</td>  
                                            <td>{labItem.technician}</td>                                         
                                            <td>{labItem.care_provider_fname + " " + labItem.care_provider_mname + " " + labItem.care_provider_lname}</td>

                                            <td>
                                                <div className="labViewButton-container">
                                                    <button id="labEditButton"
                                                             className="labViewButton"
                                                             type="button"
                                                             onClick={(e) => labEditFormRoute(e, labItem)}

                                                            //  onClick={redirectToEditForm}
                                                            // onClick={(e) => handleEdit(e, labItem)}
                                                             
                                                       
                                                    >Edit
                                                    </button>                                                       
                                                </div>     
                                                <div className="labViewButton-container">
                                                    <button id="labDeleteButton" className="labViewButton"
                                                        type="button"
                                                        onClick={(e) => handleLabList_DeleteButton(e, labItem.lab_id)}
                                                    >
                                                        Delete
                                                    </button>                                                       
                                                </div>                                                                                        
                                            </td>

                                        </tr>
                                    )

                                    

                                }
                                
                            </tbody>
                        </Table>
                          
                    </Row> 
                    {/* <Row>
                        <Form onSubmit={handleAddLabForm_SubmitButton} className="labAddForm">
                            <FormGroup>
                                
                                <p>Add New Item</p>
                                <input
                                    type="text"
                                    name="lab_date"
                                    required="required"
                                    placeholder="enter lab date"
                                    onChange={handleAddLabForm_FieldValueChange}
                                >
                                </input>
                                <input
                                    type="text"
                                    name="lab_code"
                                    required="required"
                                    placeholder="enter lab description"
                                    onChange={handleAddLabForm_FieldValueChange}
                                >
                                </input>  
                                <input
                                    type="text"
                                    name="result"
                                    required="required"
                                    placeholder="enter labo result"
                                    onChange={handleAddLabForm_FieldValueChange}
                                >
                                </input>     
                                <input
                                    type="text"
                                    name="comments"
                                    required="required"
                                    placeholder="enter comments"
                                    onChange={handleAddLabForm_FieldValueChange}
                                >
                                </input>          
                                <input
                                    type="text"
                                    name="laboratory"
                                    required="required"
                                    placeholder="enter laboratory name"
                                    onChange={handleAddLabForm_FieldValueChange}
                                >
                                </input>
                                <input
                                    type="text"
                                    name="technician"
                                    required="required"
                                    placeholder="enter technician name"
                                    onChange={handleAddLabForm_FieldValueChange}
                                >
                                </input>
                                <input
                                    type="text"
                                    name="lab_order_by"
                                    required="required"
                                    placeholder="select care provider"
                                    onChange={handleAddLabForm_FieldValueChange}
                                >
                                </input>    
                                <div className="addLab-button-container">
                                        <Button size="sm" className="addLab-add-button"
                                        onClick={(event) => handlePatientViewEditClick(event, patient)}
                                        >Add
                                        </Button>                        
                                </div>

                            </FormGroup>                                                                                                     
                        </Form>                    
                    </Row> */}
                    </section>    
                    {/* <section>
                        <div className= "labEditForm" style ={ form }>
                            <PatientLabEdit />
                        </div>

                    </section> */}
        </Container>
    )
}

export default PatientLab