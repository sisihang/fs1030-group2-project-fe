import React, { useState, useEffect } from "react";
import { useParams } from "react-router";
import { Container, Form, FormGroup, Label, Input, Row, Col, Table, Button, CardBody, CardText, Card } from 'reactstrap'
import Navigation  from '../shared/navigation'
import PatientLab from './patientLab';
import { useNavigate } from "react-router-dom";
// import "react-datepicker/dist/react-datepicker.css";


const PatientLabAdd = () => {
    const { id } = useParams();
    const navigate = useNavigate();

    const patientId = id;
    const [labAddFormData, setlabAddFormData] = useState({
        lab_date: '', 
        laboratory: '', 
        lab_code: '',
        technician : '', 
        result: '', 
        comments: '', 
        patient_id: id,
        lab_order_by: ''
    })


    const redirectToLabList = (event) => {
        event.preventDefault();
        navigate(`/patients/${id}/lab`);
    };

   // Handles changes in Add form 
   const handleAddLabForm_FieldValueChange = (event) => {
        event.preventDefault();
        const labFieldName = event.target.getAttribute("name");
        const labFieldValue = event.target.value;

        const newlabFormData = { ...labAddFormData };
        newlabFormData[labFieldName] = labFieldValue
        setlabAddFormData(newlabFormData)
    }


    // Handles the submit button in Add Form
    const handleAddLabForm_SubmitButton = (event) => {
        event.preventDefault();
        fetch(`http://localhost:4000/api/patient_laboratory`, {
            method: "post",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
            },
            //make sure to serialize your JSON body
            body: JSON.stringify(labAddFormData),
          }).then((response) => response.json());
          navigate(`/patients/${patientId}/lab`);
    };   

    return(
        <Container style={{ backgroundColor: "#ADD8E6" }} className="my-5">
         <Navigation patientID={id} />  
            <Row className="my-5">
                 <div className="editContactDetails-maincontainer">
                    <Form onSubmit = {handleAddLabForm_SubmitButton}>
                        <Row>
                            <div className="editContactDetails-cardTitle-container">
                            <Col lg="6" sm="6" xs="12">
                                <div className="editContactDetails-header-container">
                                    Patient Laboratory Input Form
                                </div>    
                            </Col>
                            <Col lg="6" sm="6" xs="12">              
                                <div className="editContactDetails-button-container">
                                    <Button size="sm" className="editContactDetails-save-button"
                                    >
                                        Submit
                                    </Button>

                                    <Button size="sm" className="editContactDetails-submit-button"
                                        onClick={redirectToLabList}
                                        >
                                            Cancel
                                    </Button>                                                                           
                                </div>
                            </Col>
                            </div>
                            <br></br> <br></br>
                        </Row>

                        <Row>
                            <div className="editContactDetails-container">
                                <Col lg="6" sm="6" xs="12">
                                    <div id="editContactDetails-mainInfo-container"> 

                                        <label id="editContactDetails-label" className="editContactDetails-label">Lab Date   :</label> 
                                        <input id="editContactDetails-textbox" className="editContactDetails-textbox"
                                            type="date"
                                            name="lab_date"
                                            required="required"
                                            placeholder="enter lab date"
                                            // value={contactDetailsEditFormData.patient_fname}
                                            onChange={handleAddLabForm_FieldValueChange}
                                        ></input> <br></br>

                                        {/* Should be dropdown */}
                                        <label id="editContactDetails-label" className="editContactDetails-label">Laboratory Type  :   </label> 
                                        <input id="editContactDetails-textbox" className="editContactDetails-textbox" 
                                        type="text"
                                        name="lab_code"
                                        required="required"
                                        placeholder="enter lab description"
                                        onChange={handleAddLabForm_FieldValueChange}
                                        // value={contactDetailsEditFormData.patient_mname}                              
                                        ></input> <br></br>

                                        <label id="editContactDetails-label" className="editContactDetails-label">Result  :</label> 
                                        <input id="editContactDetails-textbox" className="editContactDetails-textbox"
                                                    type="text"
                                                    name="result"
                                                    // required="required"
                                                    placeholder="enter comments"
                                                    onChange={handleAddLabForm_FieldValueChange}
                                                    // value={contactDetailsEditFormData.patient_lname}
                                        ></input> <br></br> <br></br>    

                                        <label id="editContactDetails-label" className="editContactDetails-label">Comments  :</label> 
                                        <input id="editContactDetails-textbox" className="editContactDetails-textbox"
                                                type="text"
                                                name="comments"
                                                // required="required"
                                                placeholder="enter comments"
                                                onChange={handleAddLabForm_FieldValueChange}
                                                // value={contactDetailsEditFormData.address_line1}
                                        ></input> <br></br>     
                                        <label id="editContactDetails-label" className="editContactDetails-label">Laboratory  :</label> 
                                        <input id="editContactDetails-textbox" className="editContactDetails-textbox"
                                                type="text"
                                                name="laboratory"
                                                required="required"
                                                placeholder="enter laboratory name"
                                                onChange={handleAddLabForm_FieldValueChange}
                                                // value={contactDetailsEditFormData.address_line2}
                                        ></input> <br></br>            
                                        <label id="editContactDetails-label" className="editContactDetails-label">Technician Name  :</label> 
                                        <input id="editContactDetails-textbox" className="editContactDetails-textbox"
                                                type="text"
                                                name="technician"
                                                required="required"
                                                placeholder="enter technician name"
                                                onChange={handleAddLabForm_FieldValueChange}
                                                //   value={contactDetailsEditFormData.city}
                                        ></input> <br></br>  
                                        
                                        <label id="editContactDetails-label" className="editContactDetails-label">Lab Ordered By  :</label> 
                                        <input id="editContactDetails-textbox" className="editContactDetails-textbox"
                                                type="text"
                                                name="lab_order_by"
                                                required="required"
                                                placeholder="select care provider"
                                                onChange={handleAddLabForm_FieldValueChange}
                                                 //   value={contactDetailsEditFormData.postal_code}

                                        ></input> <br></br>  <br></br> 
                                    </div>
                                </Col>
                            </div>  
                        </Row>    
                    </Form>  
                </div>

            </Row>
            <Card className="text-white bg-secondary my-5 py-4 text-center">
                <CardBody>
                    <CardText>
                        <span className="text-white m-0"></span>
                    </CardText>
                </CardBody>
            </Card>
            
        </Container>
    )
}

export default PatientLabAdd
