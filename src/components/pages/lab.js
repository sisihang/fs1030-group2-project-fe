import React from 'react';
import { Container, Row, Col, Button, CardBody, CardText, Card } from 'reactstrap';
import Navigation  from '../shared/navigation'
import { useParams } from "react-router";
import PatientLab from './patientLab';


const Lab = () => {
    const { id } = useParams();
    return(
        <Container style={{ backgroundColor: "#ADD8E6" }} className="my-5">
         <Navigation patientID={id} />  
            <Row className="my-5">
                {/* <Col sm="4">
                    
                </Col>
                <Col lg="5">
                    <h3 className="font-weight-light">LabratoryTest &amp; Results</h3>
                    <br />
                    <br /><br />
                </Col> */}

                    <PatientLab />                
            </Row>
            <Card className="text-white bg-secondary my-5 py-4 text-center">
                <CardBody>
                    <CardText>
                        <span className="text-white m-0"></span>
                    </CardText>
                </CardBody>
            </Card>
            
        </Container>
    )
}

export default Lab