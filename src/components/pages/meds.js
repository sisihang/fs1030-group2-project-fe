import { useEffect, useState, React } from "react";
import Navigation  from '../shared/navigation'
import { useParams } from "react-router";
import { useNavigate } from 'react-router-dom';
import {
  Container,
  Row,
  Col,
  Button,
  CardBody,
  CardText,
  Card,
  Table,
} from "reactstrap";

const Meds = () => {
  const { id } = useParams();
  const [meds, setMeds] = useState([]);
  const [patients, setPatient] = useState([]);
  const navigate = useNavigate();

  const handleDelete = (event, med) => {
    event.preventDefault();
    fetch(
      `http://localhost:4000/patient_medication/${med.medication_id}`,
      {
        method: "delete",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      }
    ).then((response) => response.json());
    window.location.reload(false);
  };

  useEffect(() => {
    const getData = async () => {
      const medResponse = await fetch(
        `http://localhost:4000/patient_medication/${id}`
      );
      const medData = await medResponse.json();
      setMeds(medData);

        const patientResponse = await fetch(`http://localhost:4000/api/patients/${id}`);
        const patientData = await patientResponse.json();
        setPatient(patientData);
    };
    getData();
  }, []);

  const redirectAdd = (event) => {
    event.preventDefault();
    navigate(`/patients/${id}/meds/add`);
  };


  return (
    <Container style={{ backgroundColor: "#ADD8E6" }} className="my-5">
      <Navigation patientID={id} />  
      <Row style={{ padding: "10px" }}>
        {patients.map((patient) => (
          <h3>
            Patient Name:{" "}
            {patient.first_name +
              " " +
              patient.middle_name +
              " " +
              patient.last_name}
          </h3>
        ))}
      </Row>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Medication Name</th>
            <th>Instruction</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {meds.length > 0 &&
            meds.map((med) => (
              <tr>
                <td>{med.medication_name}</td>
                <td>{med.instruction}</td>
                <td>                  <Button
                    onClick={(e) => {
                      handleDelete(e, med);
                    }}
                    size="sm"
                  >
                    Delete
                  </Button></td>
              </tr>
            ))}
        </tbody>
      </Table>
      <div className="text-center">
        <Button onClick={redirectAdd} className>Add New</Button>
      </div>
    </Container>
  );
};

export default Meds;
