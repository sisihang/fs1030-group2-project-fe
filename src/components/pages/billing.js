import { useEffect, useState, React } from "react";

import { Container, Row, Col, Button, CardBody, CardText, Card, FormGroup, Form, Label, Input, Table } from 'reactstrap';
import '../styles/billing.css';
import Navigation  from '../shared/navigation'
import { useParams } from "react-router";
import { useNavigate, Link } from "react-router-dom";

const Billing = (props) => {
    const { id } = useParams();
    const [costs, setCosts] = useState([]);
    const [patients, setPatients] = useState([]);

const navigate = useNavigate();

const handleDelete = (event, invoice) => {
  event.preventDefault();
  fetch(
    `http://localhost:4000/billing/${invoice.bill_invoice_id}`,
    {
      method: "delete",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    }
  ).then((response) => response.json());
  window.location.reload(false);
};

const redirectEdit = (event) => {
  event.preventDefault();
  navigate(`/patients/${id}/billing/edit`);
  
};

  useEffect(() => {
    const getData = async () => {
      const costResponse = await fetch(
        `http://localhost:4000/billing/${id}`
      );
      const costData = await costResponse.json();
      setCosts(costData);

      const patientResponse = await fetch(
        `http://localhost:4000/api/patients/${id}`
      );
      const patientData = await patientResponse.json();
      setPatients(patientData);
    };
    getData();
  }, []);

  const redirectAdd = (event) => {
    event.preventDefault();
    navigate(`/patients/${id}/billing/add`);
  };

  
  
    
        return (
            <Container style={{ backgroundColor: "#C4A484" }} className="my-5">
                               <Navigation patientID={id} />  

            <Row style={{ padding: '30px' }}>
                {patients.map((patient) => (
                    <h5>
                        Invoice:{" "}
                        {patient.first_name +
                            " " +
                            patient.middle_name +
                            " " +
                            patient.last_name}
                            <br /> Patient ID: {" "}
                            {patient.patient_id}
                    </h5>
                    ))}
            </Row>
            <Table striped bordered hover>
        <thead>
          <tr>
            <th>Bill Invoice</th>
            <th>Care Provider ID</th>
            <th>Service Rendered</th>
            <th>Service Date</th>
            <th>Billing Date</th>
            <th>Due Date</th>
            <th>Total Amount</th>
            <th>Payment</th>
            <th>Balance</th>
            <th></th>
           
          </tr>
        </thead>
        <tbody>
          {costs.length > 0 && 
            costs.map((invoice) => (
              <tr key={invoice.bill_invoice_id}>
                <td>{invoice.bill_invoice_id}</td>
                <td>{invoice.care_provider_id}</td>
                <td>{invoice.service_rendered}</td>
                <td>{invoice.service_date}</td>
                <td>{invoice.billing_date}</td>
                <td>{invoice.due_date}</td>
                <td>{invoice.total_amount}</td>
                <td>{invoice.payment}</td>
                <td>{invoice.balance}</td>
                <td>
                  {" "}
                  <Button
                    onClick={(e) => {
                      handleDelete(e, invoice);
                    }}
                    size="sm"
                  >
                    Delete
                  </Button>
                </td>
            
              </tr>
            ))}
        </tbody>
      </Table>
      <div className="text-center">
        <span className="px-3"><Button onClick={redirectAdd}>Add</Button></span>
        <Button onClick={redirectEdit}>Edit</Button> 
      </div>     
           
           
            </Container>
          )
        }
    

export default Billing;