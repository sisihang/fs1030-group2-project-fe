import React, { useState, useEffect } from "react";
import { useParams } from "react-router";
import { useNavigate } from "react-router-dom";
import {
    Button,
    Form,
    FormGroup,
    Label,
    Input,
    FormText,
    Container,
    Row,
  } from "reactstrap";
//   import '../../App.css';

const MedHistoryEdit = () => {
  const { id } = useParams();
  const patientId = id;
  const navigate = useNavigate();
  const [medHistory, setMedHistory] = useState([]);
  const [form, setForm] = useState({ display: "none" });
  const [history, setHistory] = useState({ medical_history_date: "",  major_illness: "", surgical_procedure: "", comments: "",});
  const [patients, setPatients] = useState([]);

  useEffect(() => {
    const getData = async () => {
        const patientResponse = await fetch(
            `http://localhost:4000/api/patients/${id}`
          );
          const patientData = await patientResponse.json();
          setPatients(patientData);
    };
    getData();
  }, []);

  useEffect(() => {
    async function fetchData() {
      const res = await fetch(`http://localhost:4000/medical_history/${id}`);
      
      res
        .json()
        .then((res) => setMedHistory(res))
        .catch((err) => console.log(err));

    }
    fetchData();
  }, []);

  const handleEdit = (event, med) => {
    event.preventDefault();
    setForm({ display: "block" });
    setHistory(med);
    
  };

 

    const handleChange = (event) => {
        setHistory((prevState) => ({
        ...prevState,
        [event.target.name]: event.target.value,
      }));
    };

      const handleSubmit = (event) => {
        event.preventDefault();
        // console.log(id);
        fetch(`http://localhost:4000/medical_history/${id}`, {
          method: "put",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
          },

          //make sure to serialize your JSON body
          body: JSON.stringify(history),
        }).then((response) => response.json());
        navigate(`/patients/${patientId}/medHistory`);
      };

  return (
      
<Container style={{ backgroundColor: "rgb(162, 252, 252)" }}>

      <Row style={{ padding: '30px' }}>
        {patients.map((patient) => (
          <h5>
            Edit Medical History - Name:{" "}
            {patient.first_name +
              " " +
              patient.middle_name +
              " " +
              patient.last_name + " "}
            <br /> Patient ID: {" "}
            {patient.patient_id}
          </h5>
        ))}
      </Row>
      <Row className="m-3">
        {medHistory.map((med) => (
          <div key={med.id}>
            <p className="ml-3">Medical History ID: &nbsp; {med.medical_history_id} &nbsp; &nbsp; &nbsp; &nbsp;
             Date: &nbsp;{med.medical_history_date} 
           
            <button className="mx-3 px-3 " onClick={(e) => handleEdit(e, med)}>Edit</button> </p>
          </div>
        ))}
      </Row>
      <Form className="m-3" onSubmit={(e) => handleSubmit(e)} style={form} >
     
      <FormGroup >
          <Label>Invoice No.:</Label>
          <Input
           type="text"
           name="medical_history_id"
           value = {history.medical_history_id}
           disabled />
        </FormGroup>
        <FormGroup >
          <Label>Care Provider</Label>
          <Input
           type="text"
           name="care_provider_id"
           value = {history.care_provider_id}
           disabled />
        </FormGroup> <FormGroup>
          <Label>Date:</Label>
            <Input
              type="date"
              name="medical_history_date"
              value={history.medical_history_date}
              onChange={handleChange}
            />
        </FormGroup>
        <FormGroup>
          <Label>Major Illness:</Label>
            <Input
              type="text"
              name="major_illness"
              value={history.major_illness}
              onChange={handleChange}
            />
        </FormGroup>
        <FormGroup>
          <Label>Surgical Procedure:</Label>
            <Input
              type="text"
              name="surgical_procedure"
              value={history.surgical_procedure}
              onChange={handleChange}
            />
        </FormGroup>
        <FormGroup>
          <Label for="comments">Comments:</Label>
            <Input
              type="textarea"
              name="comments"
              value={history.comments}
              onChange={handleChange}
            />
        </FormGroup>

        <Button type="submit" value="submit">Submit</Button>
      </Form >
    </Container>
  );
};




export default MedHistoryEdit;
