import React, { useState,useHistory, useParams, useEffect} from 'react';
import { Form, FormGroup, Col, Row, Input, Label, Button, Container, CardBody, Card, CardText } from 'reactstrap';
import {  useLocation,} from 'react-router-dom'
import axios from "axios";
import'../styles/admin.css';



const EditCareprovider = () => {

    let history = useHistory(); //The useHistory hook gives you access to the history instance that you may use to navigate.
    const { id } = useParams(); //The useParams() hook helps us to access the URL parameters from a current route.
  
    const [user, setUser] = useState({
      fname: "",
      mname: "",
      lname: "",
      email: "",
      cpsonumber: "",
      initialsetup: "",
      careprovidertypeid: ""
    });

    const {
    fname, mname, lname, email, cpsonumber, initialsetup, careprovidertypeid
    } = user;
    const onInputChange = (e) => {
      setUser({ ...user, [e.target.name]: e.target.value });
    };
  
    useEffect(() => {
      loadUser();
    }, []);
  
    const updateCareprovider = async (e) => {
      e.preventDefault();
      await axios.put(`http://localhost:4000/api/v1/careprovider/${id}`, user);
      history.push("/");
    };
  
    const loadUser = () => {
      fetch(`http://localhost:4000/api/v1/careprovider/${id}`, {
        method: "GET",
      })
        .then((response) => response.json())
        .then((result) => {
          console.log(result);
          setUser({
            id: id,
            update: true,
            fname: result.response[0].first_name,
            mname: result.response[0].middle_name,
            lname: result.response[0].last_name,
            email: result.response[0].email,
            cpsonumber: result.response[0].cpso_number,
            initialsetup: result.response[0].initial_setup,
            careprovidertypeid: result.response[0].care_provider_type_id,

          });
        })
        .catch((error) => console.log("error", error));
    };
  
    
    return (
      <div className="container">
        <div className="row mt-4">
          <div className="col-sm-5 col-offset-3 mx-auto shadow p-5">
            <h4 className="text-center mb-4">Edit A Care Provider</h4>
  
            <h5 className="text-success">CareProvider ID: {user.id} </h5>
  
  
            <div className="form-group mb-3">
              <input
                type="text"
                className="form-control form-control-lg"
                placeholder="Enter Careprovider Name"
                name="fname"
                value={fname}
                onChange={(e) => onInputChange(e)} />
            </div>
  
            <div className="form-group mb-3">
              <input
                type="text"
                className="form-control form-control-lg"
                placeholder="Enter Careprovider Name"
                name="mname"
                value={mname}
                onChange={(e) => onInputChange(e)} />
            </div>
  
  
            <div className="form-group mb-3">
              <input
                type="text"
                className="form-control form-control-lg"
                placeholder="Enter Careprovider Name"
                name="lname"
                value={lname}
                onChange={(e) => onInputChange(e)} />
            </div>

            <div className="form-group mb-3">
              <input
                type="text"
                className="form-control form-control-lg"
                placeholder="Enter your Email"
                name="email"
                value={email}
                onChange={(e) => onInputChange(e)} />
            </div>
  
            <div className="form-group mb-3">
              <input
                type="text"
                className="form-control form-control-lg"
                placeholder="Enter cpso number"
                name="cpsonumber"
                value={cpsonumber}
                onChange={(e) => onInputChange(e)} />
            </div>
  
            <div className="form-group mb-3">
              <input
                type="text"
                className="form-control form-control-lg"
                placeholder="Enter Initial Setup"
                name="initialsetup"
                value={initialsetup}
                onChange={(e) => onInputChange(e)} />
            </div>
  
            <div className="form-group mb-3">
              <input
                type="text"
                className="form-control form-control-lg"
                placeholder="Enter Care Provider Type Id"
                name="careprovidertypeid"
                value={careprovidertypeid}
                onChange={(e) => onInputChange(e)} />
            </div>
  
          <button onClick={updateCareprovider} className="btn btn-secondary btn-block">Update CareProvider</button>
       
       </div>
            
            
          </div>
  
        </div>
    
  
  );
};
 
    
  

    export default EditCareprovider
   