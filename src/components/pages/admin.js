import React, { useState, useEffect } from "react";
import { Form, FormGroup, Col, Row, Input, Label, Button, Container, CardBody, Card, CardText } from 'reactstrap';
import { useNavigate } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from "axios";
import { Link } from 'react-router-dom';

function PatientDetail()

{
  const [search, setSearch] = useState('');
  const [record, setRecord] = useState([]);

  const [user, setUser] = useState({
    HealthCardno: "",
    HealthCardver: "",
    fname: "",
    mname: "",
    lname: "",
    addressline1: "",
    addressline2: "",
    city: "",
    provincecode: "",
    postalcode: "",
    homephone: "",
    cellphone: "",
    email: "",
    familydoctorcode: ""
  });  


      

  //  Object Destructuring 
  const { HealthCardno, HealthCardver, fname, mname, lname, addressline1, addressline2, city, provincecode, postalcode, homephone, cellphone, email, familydoctorcode } = user;
  const onInputChange = e => {
    setUser({ ...user, [e.target.name]: e.target.value });
  };


  // On Page load display all records 
  const loadPatientDetail =  () => {
     fetch('http://localhost:4000/api/v1/patient')
      .then(function (response) {
        return response.json();
      })
      .then(function (myJson) {
        setRecord(myJson);
      });
  }

  useEffect(() => {
    loadPatientDetail();
  }, []);

  // Insert Patient Records 
  const submitPatientRecord = async (e) => {
    e.preventDefault();
    e.target.reset();
    await axios.post("http://localhost:4000/api/v1/patient", user);
    alert('Data Inserted');

    loadPatientDetail();
  };

  // Search Records here 

  const searchRecords = () => {
    alert(search)
    axios.get(`http://localhost:4000/api/v1/patient/searchRecord/${search}`)
      .then(response => {
        setRecord(response.data);
      });
  }

  // Delete Patient Record

  const deleteRecord = (e, productId) => {
    e.preventDefault();
    axios.delete(`http://localhost:4000/api/v1/patient/${productId}`)
      .then((result) => {
        loadPatientDetail();
      })
      .catch(() => {
        alert('Error in the Code');
      });
  };

  return (
    <section>


      <nav class="navbar navbar-expand-lg navbar-light bg-dark">
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
          <ul class="navbar-nav">
            <li class="nav-item active ">
            </li>
            <li class="nav-item active">
              <a class="nav-link text-white" href="/careadmin">Edit Care Providers <span class="sr-only">(current)</span></a>
            </li>
          </ul>
        </div>
      </nav>

      <div class="container">
        <div class="row mt-3">
          <div class="col-sm-4">
            <div className="box p-3 mb-3 mt-5" style={{ border: "1px solid #d0d0d0" }}>
              <form onSubmit={submitPatientRecord}>
                <h5 className="mb-3 ">Insert Patient Records</h5>

                <div class="form-group">
                  <input type="text" class="form-control  mb-4" name="HealthCardno" value={HealthCardno} onChange={e => onInputChange(e)} placeholder="Enter HealthCardno" required="" />
                </div>

                <div class="form-group">
                  <input type="text" class="form-control  mb-4" name="HealthCardver" value={HealthCardver} onChange={e => onInputChange(e)} placeholder="Enter HealthCardver" required="" />
                </div>

                <div class="form-group">
                  <input type="text" class="form-control  mb-4" name="fname" value={fname} onChange={e => onInputChange(e)} placeholder="Enter name" required="" />
                </div>

                <div class="form-group">
                  <input type="text" class="form-control  mb-4" name="mname" value={mname} onChange={e => onInputChange(e)} placeholder="Enter middle name" required="" />
                </div>

                <div class="form-group">
                  <input type="text" class="form-control  mb-4" name="lname" value={lname} onChange={e => onInputChange(e)} placeholder="Enter lastname" required="" />
                </div>

                <div class="form-group">
                  <input type="text" class="form-control mb-4" name="addressline1" value={addressline1} onChange={e => onInputChange(e)} placeholder="Enter addressline1" required="" />
                </div>

                <div class="form-group">
                  <input type="text" class="form-control mb-4" name="addressline2" value={addressline2} onChange={e => onInputChange(e)} placeholder="Enter addressline2" required="" />
                </div>

                <div class="form-group">
                  <input type="text" class="form-control mb-4" name="city" value={city} onChange={e => onInputChange(e)} placeholder="Enter City" required="" />
                </div>

                <div class="form-group">
                  <input type="text" class="form-control mb-4" name="provincecode" value={provincecode} onChange={e => onInputChange(e)} placeholder="Enter Provincecode" required="" />
                </div>

                <div class="form-group">
                  <input type="text" class="form-control mb-4" name="postalcode" value={postalcode} onChange={e => onInputChange(e)} placeholder="Enter Postalcode" required="" />
                </div>

                <div class="form-group">
                  <input type="text" class="form-control mb-4" name="homephone" value={homephone} onChange={e => onInputChange(e)} placeholder="Enter HomePhone" required="" />
                </div>

                <div class="form-group">
                  <input type="text" class="form-control mb-4" name="cellphone" value={cellphone} onChange={e => onInputChange(e)} placeholder="Enter CellPhone" required="" />
                </div>

                <div class="form-group">
                  <input type="text" class="form-control mb-4" name="email" value={email} onChange={e => onInputChange(e)} placeholder="Enter Email" required="" />
                </div>

                <div class="form-group">
                  <input type="text" class="form-control mb-4" name="familydoctorcode" value={familydoctorcode} onChange={e => onInputChange(e)} placeholder="Familydoctorcode" required="" />
                </div>

                <button type="submit" class="btn btn-primary btn-block mt-4">Insert Record</button>
          </form>
            </div>
          </div>
          <div class="col-sm-8">
            <h4 class="text-center  ml-4 mt-4  mb-5">View Patients</h4>
            <div class="input-group mb-4 mt-3">
              <div class="form-outline">
                <input type="text" id="form1" onChange={(e) => setSearch(e.target.value)} class="form-control" placeholder="Search Patient Here" style={{ backgroundColor: "#ececec" }} />
              </div>
              <button type="button" onClick={searchRecords} class="btn btn-success">
                <i class="fa fa-search" aria-hidden="true"></i>
              </button>
            </div>
            <table class="table table-hover  table-striped table-bordered ml-4 ">
              <thead>
                <tr>
                <th>Health Cardno</th>
                  <th>Health Cardver</th>
                  <th>First Name</th>
                  <th>Middle Name</th>
                  <th>Last Name</th>
                  <th>Addressline1</th>
                  <th>Addressline2</th>
                  <th>City</th>
                  <th>Province code</th>
                  <th>Postal Code</th>
                  <th>Home Phone</th>
                  <th>Cell Phone</th>
                  <th>Email</th>
                  <th>Family Doctor Code</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>

                {record.map((name) =>
                  <tr>
                    <td>{name.healthcard_no}</td>
                    <td>{name.healthcard_ver}</td>
                    <td>{name.first_name}</td>
                    <td>{name.middle_name}</td>
                    <td>{name.last_name}</td>
                    <td>{name.address_line1}</td>
                    <td>{name.address_line2}</td>
                    <td>{name.city}</td>
                    <td>{name.province_code}</td>
                    <td>{name.postal_code}</td>
                    <td>{name.home_phone}</td>
                    <td>{name.cell_phone}</td>
                    <td>{name.email}</td>
                    <td>{name.family_doctor_code}</td>
                    <td>
                  
                      <a className="text-danger mr-2"
                        onClick={(e) => {
                          const confirmBox = window.confirm(
                            "Do you really want to delete " + name.first_name
                          )
                          if (confirmBox === true) {
                            deleteRecord(e, name.patient_id)
                          }
                        }}> <i class="far fa-trash-alt"  style={{ fontSize: "18px", marginRight: "5px" }}></i> </a>

                      <Link class=" mr-2" to={`/EditPatient/${name.patient_id}`}>
                        <i class="fa fa-edit" aria-hidden="true"></i>
                      </Link>
                    </td>
                  </tr>
                )}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </section>
  )
}

export default PatientDetail;