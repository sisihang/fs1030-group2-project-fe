import React, { useEffect, useState } from 'react'
import { Container, Form, FormGroup, Label, Input, Row, Col, Table, Button } from 'reactstrap'
// import { useHistory } from "react-router-dom";
import { useNavigate } from 'react-router-dom';
import { useParams } from "react-router";
import "../styles/patientLab.css"


const PatientNotes = () => {

    const { id } = useParams();
    const patientID = id;

    const [notesList, setNotesList] = useState([]);
    const [notesAddFormData, setNotesAddFormData] = useState({
        // chart_entry_date: '', 
        comments : '', 
        care_provider_id: '',        
        patient_id: patientID,
    })

    const [searchNote, setSearchNote] = useState([])

    const navigate = useNavigate();

    // Function to format date
    const formatDateToMMDDYYYY = (dateFieldToFormat) => {
        const formattedDate = new Date(dateFieldToFormat).toLocaleDateString();
        return formattedDate;
    } 


    useEffect(() => {
        const fetchData = async () => {
          const notesRes = await fetch(`http://localhost:4000/api/patient_notes/${patientID}`);
          const notesData = await notesRes.json();
          setNotesList(notesData);
        };
        fetchData();
      }, [patientID]);

   

    const redirectToNotesAddForm = (event) => {
        event.preventDefault();
        navigate(`/patients/${id}/notes/add`);
    };    


    const handleNotesSearchChange = event => {
        setSearchNote(event.target.value);
    };    


   // Handles input in ADD form 
    const handleAddNotesForm_FieldValueChange = (event) => {
        event.preventDefault();
        const labFieldName = event.target.getAttribute("name");
        const labFieldValue = event.target.value;
        const newNoteFormData = { ...notesAddFormData };
        newNoteFormData[labFieldName] = labFieldValue
        setNotesAddFormData(newNoteFormData)
    }


    // Handles the SUBMIT button in ADD Form
    const handleAddLabForm_SubmitButton = () => {
    fetch(`http://localhost:4000/api/patient_notes`, {
        method: "POST",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
        },
        //make sure to serialize your JSON body
        body: JSON.stringify(notesAddFormData),
        }).then((response) => response.json());
        navigate(`/patients/${id}/note`);
    }; 


   // Handles the EDIT button in the NOTES LITS 
   const notesEditFormRoute = (event, noteItem) => {
    event.preventDefault();
    navigate(`/patients/${id}/notes/edit/${noteItem.chart_entry_id}`);        
  }

 

    return (
        <Container>
                <section className="lab-listing-container">
                    <Row>
                        <Col xs={12} sm={12} md={12} lg={12}>  
                        <Button size="sm" 
                                style={{fontSize: ".8rem", float: "right", marginBottom: "2%"}}
                                onClick = {redirectToNotesAddForm}
                        >
                            Add
                        </Button>
                    </Col>
                    </Row>
                    <Row>
                        
                        <Table striped hover bordered responsive size="sm" >
                            
                            <thead>
                                <tr>
                                    <th className="w-1" >ID</th>
                                    <th className="column-lab-date">Time Stamp</th>   
                                    <th className={"w-65"}>Comments</th>  
                                    <th className={"w-1"}>Care Provider</th>
                                    <th className={"w-15"}>Action</th>

                                </tr>
                            </thead>
                            <tbody>
                                {notesList.length === 0 &&
                                    <tr><td colSpan="4" className="text-center"><i>There is no notes found for this patient!</i></td></tr>
                                }
                                {notesList.length > 0 &&
                                    notesList.filter((noteItem) => {
                                        if (searchNote === "") {
                                            return noteItem
                                        } else if(noteItem.comments.includes(searchNote)) {
                                            return noteItem

                                        }
                                    }).map(noteItem => 
                                        <tr key={noteItem.chart_entry_id}>
                                            <td style = {{ textAlign: "center"}} >{noteItem.chart_entry_id}</td>
                                            <td>{noteItem.chart_entry_date}</td>
                                            <td>{noteItem.comments}</td>
                                            <td>{noteItem.care_provider_fname + " " + noteItem.care_provider_mname + " " + noteItem.care_provider_lname}</td> 
                                            <td>
                                                <div className="labViewButton-container">
                                                    <button id="labEditButton"
                                                                className="labViewButton"
                                                                type="button"
                                                                onClick={(e) => notesEditFormRoute(e, noteItem)}
                                                    >Edit
                                                    </button>                                                       
                                                </div>                                                                                            
                                            </td>
                                        </tr>
                                    )
                                }
                                
                            </tbody>
                        </Table>
                    </Row> 
    
                </section>    
        </Container>
    )
}

export default PatientNotes
