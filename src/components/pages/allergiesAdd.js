import { useEffect, useState, React } from "react";
import { useParams } from "react-router";
import {
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  FormText,
  Container,
  Row,
} from "reactstrap";
import { useNavigate } from "react-router-dom";

const AllergiesAdd = () => {
  const { id } = useParams();
  const patientId = id;
  const [allergyname, setAllergyName] = useState("");
  const [serverity, setServerity] = useState("");
  const [comments, setComments] = useState("");
  const [careProviderId, setCareProviderId] = useState("");

  const navigate = useNavigate();
  useEffect(() => {
    const getData = async () => {
      const patientRes = await fetch(
        `http://localhost:4000/api/patients/${patientId}`
      );
      const res = await patientRes.json();
      setCareProviderId(res[0].family_doctor_code);
    };
    getData();
  }, []);

  const formSubmit = async (event) => {
    event.preventDefault();
    const response = await fetch("http://localhost:4000/patient_allergy", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        allergyname,
        serverity,
        comments,
        patientId,
        careProviderId,
      }),
    });
    if (response.status === 201) {
      alert(`Congrats! Submission submitted`);
    }
    navigate(`/patients/${patientId}/allergies`);
  };

  return (
    <Container style={{ backgroundColor: "#B19CD9" }} className="my-5">
      <Form>
        <h3 style={{ padding: "10px", textAlign: "center" }} className="font">
          Add a new entry
        </h3>
        <FormGroup>
          <Label for="allergyname">Allergy Name</Label>
          <Input
            type="text"
            name="allergyname"
            id="allergyname"
            placeholder="Allergy Name"
            onChange={(e) => setAllergyName(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label for="serverity">Serverity</Label>
          <Input
            type="text"
            name="serverity"
            id="serverity"
            placeholder="Serverity"
            onChange={(e) => setServerity(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label for="comments">Comments</Label>
          <Input
            type="textarea"
            name="comments"
            id="comments"
            placeholder="comments"
            onChange={(e) => setComments(e.target.value)}
          />
        </FormGroup>
      </Form>
      <div style={{ padding: "10px", textAlign: "center" }}>
        <Button onClick={formSubmit}>Submit</Button>
      </div>
    </Container>
  );
};

export default AllergiesAdd;
