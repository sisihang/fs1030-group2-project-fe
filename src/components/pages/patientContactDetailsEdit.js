import React, { useState, useEffect } from "react";
import { useParams } from "react-router";
import { NavLink as RouteLink } from "react-router-dom";
import { Container, Form, FormGroup, Label, Input, Row, Col, Table, Button } from 'reactstrap'
// import Select from 'react-select'

import "../styles/patientContactDetailsReadOnly.css"
import "../styles/patientEditContactDetails.css"
import "../styles/patientDemographicsReadOnly.css"
import "../styles/patientDemographicsEdit.css"
// import ProvinceLookup from "./lookup/ProvinceLookup-toremove";


const PatientContactDetailsEdit = ({patient, contactDetailsEditFormData, handleContactDetailsEditFormChange, handleContactDetailsEditCancelClick, handleProvinceChange, selectedProvince, provinceLookupValue}) => {

  // const [provinceLookupValue, setProvinceLookupValue] = useState([])
  // const [selectedProvince, setSelectedProvince] = useState({
  //       province_code: patient.province_code,
  //       province_name: patient.province_name
  // })

//   useEffect(() => {
//     async function fetchData() {
//       const res = await fetch("http://localhost:3001/api/province");
//       res
//         .json()
//         .then((res) => setProvinceLookupValue(res))
//         .catch((err) => console.log(err));
//     }
//       fetchData();
//   }, []);

// const handleProvinceChange = (e) => {
//   setSelectedProvince({province_name: e.target.value})
// }

    // useEffect(() => {
    //   async function fetchData() {
    //     const res = await fetch("http://localhost:3001/api/province");
    //     res
    //       .json()
    //       .then((res) => setProvinceLookupValues(res))
    //       .catch((err) => console.log(err));
    //   }
    //       fetchData();
    //   }, []);


    return (
        <div className="editContactDetails-maincontainer">
          {/* <Form> */}
            <Row>
                <div className="editContactDetails-cardTitle-container">
                  <Col lg="6" sm="6" xs="12">
                    <div className="editContactDetails-header-container">
                          Contact Details
                    </div>    
                  </Col>
                  <Col lg="6" sm="6" xs="12">              
                    <div className="editContactDetails-button-container">
                          <Button size="sm" className="editContactDetails-save-button"
                            // onClick={(e) => handleEdit_ContactDetails(e, patient)}
                          >Save
                          </Button>

                          <Button size="sm" className="editContactDetails-submit-button"
                              onClick={handleContactDetailsEditCancelClick}
                            >Cancel
                         </Button>                                                                           
                    </div>
                  </Col>
                  </div>
                  <br></br> <br></br>
              </Row>

              <Row>
                <div className="editContactDetails-container">
                    <Col lg="6" sm="6" xs="12">
                        <div id="editContactDetails-mainInfo-container"> 
                            <label id="editContactDetails-label" className="editContactDetails-label">First Name   :</label> 
                            <input id="editContactDetails-textbox" className="editContactDetails-textbox"
                                type="text"
                                // required="required"
                                name="first_name"
                                value={contactDetailsEditFormData.first_name}
                                onChange={handleContactDetailsEditFormChange}
                            ></input> <br></br>

                            <label id="editContactDetails-label" className="editContactDetails-label">Middle Name  :   </label> 
                            <input id="editContactDetails-textbox" className="editContactDetails-textbox" 
                                type="text"
                                // required="required"
                                name="middle_name"
                                value={contactDetailsEditFormData.middle_name}
                              // value={editContactDetailsFormData.middleName}                                  
                                 onChange={handleContactDetailsEditFormChange}
                            ></input> <br></br>

                            <label id="editContactDetails-label" className="editContactDetails-label">Last Name  :</label> 
                            <input id="editContactDetails-textbox" className="editContactDetails-textbox"
                                    type="text"
                                    // required="required"
                                    name="last_name"
                                    value={contactDetailsEditFormData.last_name}
                                    onChange={handleContactDetailsEditFormChange}
                            ></input> <br></br> <br></br>    

                            <label id="editContactDetails-label" className="editContactDetails-label">Address Line1  :</label> 
                            <input id="editContactDetails-textbox" className="editContactDetails-textbox"
                                type="text"
                                // required="required"
                                name="address_line1"                            
                                value={contactDetailsEditFormData.address_line1}
                                onChange={handleContactDetailsEditFormChange}
                            ></input> <br></br>     
                            <label id="editContactDetails-label" className="editContactDetails-label">Address Line2  :</label> 
                            <input id="editContactDetails-textbox" className="editContactDetails-textbox"
                                type="text"
                                // required="required"
                                name="address_line2"                            
                                value={contactDetailsEditFormData.address_line2}
                                onChange={handleContactDetailsEditFormChange}
                            ></input> <br></br>            
                            <label id="editContactDetails-label" className="editContactDetails-label">City  :</label> 
                            <input id="editContactDetails-textbox" className="editContactDetails-textbox"
                                  type="text"
                                  // required="required"
                                  name="city"
                                  value={contactDetailsEditFormData.city}
                                  onChange={handleContactDetailsEditFormChange}
                            ></input> <br></br>  
                            
                            {/* // These code can select from dropdown but does not display the province state default value */}
                            <label id="editContactDetails-label" className="editContactDetails-label">Province  :</label> 
                            <select                           
                                    id="editContactDetails-textbox" 
                                    className="editContactDetails-textbox"
                                    onChange = {handleProvinceChange}
                             >
                                 {provinceLookupValue.map( (province) => (  
                                     <option key={province.index} 
                                             value={province.province_code}
                                             selected={(selectedProvince.province_code === province.province_code) ? 'selected' : ''}
                                     >{province.province_name}</option>
                                 ))}
                            </select> <br></br>                              
                          
                              <label id="editContactDetails-label" className="editContactDetails-label">Postal Code  :</label> 
                              <input id="editContactDetails-textbox" className="editContactDetails-textbox"
                                  type="text"
                                  // required="required"
                                  name="postal_code"                                
                                  value={contactDetailsEditFormData.postal_code}
                                  onChange={handleContactDetailsEditFormChange}
                              ></input> <br></br>  <br></br> 
                        </div>
                    </Col>

                    <Col lg="6" sm="6" xs="12">
                        <div id="editContactDetails-workInfo-container">

                                <label id="editContactDetails-label" className="editContactDetails-label">Home Phone  :</label> 
                                <input id="editContactDetails-textbox" className="editContactDetails-textbox"
                                    type="text"
                                    // required="required"
                                    name="home_phone"                                
                                    value={contactDetailsEditFormData.home_phone}
                                    onChange={handleContactDetailsEditFormChange}
                                ></input> <br></br>  
                                  
                                <label id="editContactDetails-label" className="editContactDetails-label">Cell Phone  :</label> 
                                <input id="editContactDetails-textbox" className="editContactDetails-textbox"
                                    type="text"
                                    // required="required"
                                    name="cell_phone"                                
                                    value={contactDetailsEditFormData.cell_phone}
                                    onChange={handleContactDetailsEditFormChange}
                                ></input> <br></br>   

                                <label id="editContactDetails-label" className="editContactDetails-label">Email  :</label> 
                                <input id="editContactDetails-textbox" className="editContactDetails-textbox"
                                      type="text"
                                      // required="required"
                                      name="email"
                                      value={contactDetailsEditFormData.email}
                                      onChange={handleContactDetailsEditFormChange}
                                ></input> <br></br> 

                                {/* Removing Family Doctor - can be done in Admin only */}
                                {/* <label id="editContactDetails-label" className="editContactDetails-label">Family Doctor  :</label> 
                                <input id="editContactDetails-textbox" className="editContactDetails-textbox"
                                      type="text"
                                      name="family_doctor_code"
                                      value={contactDetailsEditFormData.family_doctor_code}
                                      onChange={handleContactDetailsEditFormChange}
                                ></input> <br></br>                                  */}
                        </div>
                    </Col>   
                </div>  
                 
              </Row> 
        </div>
    )
}

export default PatientContactDetailsEdit
