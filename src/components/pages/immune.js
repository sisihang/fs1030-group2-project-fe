import { useEffect, useState, React } from "react";
import {
  Container,
  Row,
  Col,
  Button,
  CardBody,
  CardText,
  Card,
  Table,
} from "reactstrap";
import "../styles/immune.css";
import Navigation from "../shared/navigation";
import { useParams } from "react-router";
import { useNavigate } from "react-router-dom";

const Immune = () => {
  const { id } = useParams();
  const navigate = useNavigate();
  const [Immunizations, setImmunizations] = useState([]);
  const [patients, setPatient] = useState([]);

  const redirectAdd = (event) => {
    event.preventDefault();
    navigate(`/patients/${id}/immune/add`);
  };

  const handleDelete = (event, immu) => {
    event.preventDefault();
    fetch(
      `http://localhost:4000/patient_immunization/${immu.immunization_id}`,
      {
        method: "delete",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      }
    ).then((response) => response.json());
    window.location.reload(false);
  };

  useEffect(() => {
    const getData = async () => {
      const immuResponse = await fetch(
        `http://localhost:4000/patient_immunization/${id}`
      );
      const immuData = await immuResponse.json();
      setImmunizations(immuData);

      const patientResponse = await fetch(
        `http://localhost:4000/api/patients/${id}`
      );
      const patientData = await patientResponse.json();
      setPatient(patientData);
    };
    getData();
  }, []);

  return (
    <Container className="bgcolor my-5">
      <Navigation patientID={id} />
      <Row style={{ padding: "10px" }}>
        {patients.map((patient) => (
          <h3>
            Patient Name:{" "}
            {patient.first_name +
              " " +
              patient.middle_name +
              " " +
              patient.last_name}
          </h3>
        ))}
      </Row>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Date</th>
            <th>Immunization Name</th>
            <th>Dose</th>
            <th>Administered By</th>
            <th>Comments</th>
            <th>Care Provider Name</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {Immunizations.length > 0 &&
            Immunizations.map((immu) => (
              <tr>
                <td>{immu.date_completed}</td>
                <td>{immu.immunization_name}</td>
                <td>{immu.dose}</td>
                <td>{immu.administer_by}</td>
                <td>{immu.comments}</td>
                <td>{immu.careProvider_name}</td>
                <td>
                  {" "}
                  <Button
                    onClick={(e) => {
                      handleDelete(e, immu);
                    }}
                    size="sm"
                  >
                    Delete
                  </Button>
                </td>
              </tr>
            ))}
        </tbody>
      </Table>
      <div className="text-center">
        <Button onClick={redirectAdd} className>
          Add New
        </Button>
      </div>
    </Container>
  );
};

export default Immune;
