import React, { useState } from 'react';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink, Container, Button, Row, CardBody, Card, CardText, Form, FormGroup, Col, Input, Label, } from 'reactstrap';
import { NavLink as RouteLink } from 'react-router-dom';
import adminLogo from '../images/admin-icon.png';

import { useParams } from "react-router";
import './navigation.css';
import admin from '../pages/admin'



const Navigation = (props) => {
    
    return (
        <div>
        
        
        <Nav fill variant="tabs" defaultactiveKey="/patient" color="light" className="my-2">
           
            <NavItem className="navTab" style={{backgroundColor:'grey'}}>
                <NavLink tag={RouteLink} to="/login" style={{color: 'white'}}>Login</NavLink>
            </NavItem>
            <NavItem className="navTab" style={{backgroundColor:'orange'}}>
                <NavLink tag={RouteLink} to={`/patients/${props.patientID}`} style={{color: 'white'}}>Patient</NavLink>
            </NavItem>
            <NavItem className="navTab" style={{backgroundColor: 'aqua'}}>
                <NavLink tag={RouteLink} to={`/patients/${props.patientID}/medHistory`} style={{color: 'white'}}>Medical History</NavLink>
            </NavItem>
            <NavItem className="navTab" style={{backgroundColor:'#345095'}}>
                <NavLink tag={RouteLink} to={`/patients/${props.patientID}/meds`}  style={{color: 'white'}}>Medication</NavLink>
            </NavItem>
            <NavItem className="navTab" style={{backgroundColor:'purple'}}>
                <NavLink tag={RouteLink} to={`/patients/${props.patientID}/allergies`} style={{color: 'white'}}>Allergies</NavLink>
            </NavItem>
            <NavItem className="navTab" style={{backgroundColor:'red'}}>
                <NavLink tag={RouteLink} to={`/patients/${props.patientID}/immune`} style={{color: 'white'}}>Immunizations</NavLink>
            </NavItem>
            <NavItem className="navTab" style={{backgroundColor:'Blue'}}>
                <NavLink tag={RouteLink} to={`/patients/${props.patientID}/lab`} style={{color: 'white'}}>Labratory Test &amp; Results</NavLink>
            </NavItem>
            <NavItem className="navTab" style={{backgroundColor:'green'}}>
                <NavLink tag={RouteLink} to={`/patients/${props.patientID}/radio`}  style={{color: 'white'}}>Radiology</NavLink>
            </NavItem>
            <NavItem className="navTab" style={{backgroundColor:'#532014', color: 'white'}}>
                <NavLink tag={RouteLink} to={`/patients/${props.patientID}/billing`}  style={{color: 'white'}}>Billing</NavLink>
            </NavItem>
            <NavItem className="navTab" style={{backgroundColor:'grey'}}>
                <NavLink tag={RouteLink} to={`/patients/${props.patientID}/notes`} style={{color: 'white'}}>Notes</NavLink>
            </NavItem>
            <NavItem className="navTab" style={{backgroundColor:'orange'}}>
                <NavLink tag={RouteLink} to={`/patients/${props.patientID}/revision_history`} style={{color: 'white'}}>Revision History</NavLink>
            </NavItem>
            <NavItem className="navTab" style={{backgroundColor:'pink'}}>
                <NavLink tag={RouteLink} to="/admin" > <img className="checkImg" src={adminLogo} alt="" /></NavLink>
            </NavItem>
        </Nav>
        </div>
    )
}

export default Navigation