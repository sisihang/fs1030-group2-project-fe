# FS1030-S21-Group2-EMR Project (Frontend Repo)

**Description:**  This is an Electronic Medical Record Management System designed to be used by a care provider to be able to pull/update patient's medical information.

**Note:** This repo is the front end portion of the project.

 The database and backend repos are needed to be able to see the \complete project package.

    Database Repo Link: https://gitlab.com/nols2021/fs1030-s21-group2-emr-database

    Backend Repo Link: https://gitlab.com/sisihang/fs1030-group2-project-be

**Technologies**

    The project has been developed using REACT, NodeJS and MySQL.
    
    Git Bash Terminal / Visual Studio Code / MySQL Workbench can be used to clone the project / work with the database.

## Getting started

    Step1: Project Repo Cloning Process
        -Open bash terminal then create a folder where to store the repo
        -Change directory to the newly created folder
        -Clone the repo
            -At GitLab
                >> Go to Repository >> Select the 'main' repo from the dropdown
             -On the right side
                 >> Right click on 'Clone' dropdown
            -Select Clone with HTTPS
            -Go back to the terminal then issue the command below (without the '<' and '>' symbols)

                git clone <paste the repo URL here>

        -Change directory to the repo folder "fs1030-group-project-fe"

# Install Dependencies

    npm install

# Start the application

    In the project directory, run the command below

        npm start

    Note: The above will run the application in the development mode. 
          You will also see line errors in the console (you can ignore them)

            Open http://localhost:3000 to view it in the browser.


# Login Page

    You can use any email or password to log in to our main application

## Routes

    Login: /
    Patients list/search: /patients
    Patient: /patients/:id
    Patient Emergency Edit: /patients/:id/emergency/edit/contactID
    MedHistory: /patients/:id/medHistory
    MedHistoryAdd: /patients/:id/medHistory/add
    MedHistoryEdit: /patients/:id/medHistory/edit
    Meds: /patients/:id/meds
    MedsAdd: /patients/:id/meds/add
    Allergies: /patients/:id/allergies
    AllergiesAdd: /patients/:id/allergies/add
    Immune: /patients/:id/immune
    ImmuneAdd: /patients/:id/immune/add
    Lab: /patients/:id/lab
    PatientLabAdd: /patients/:id/lab/add
    PatientLabEdit: /patients/:id/lab/edit/:labID
    Radio: /patients/:id/radio
    RadioAdd: /patients/:id/radiology/add
    RadioEdit: /patients/:id/radiology/edit
    Billing: /patients/:id/billing
    BillingAdd: /patients/:id/billing/add
    BillingEdit: /patients/:id/billing/edit
    Notes: /patients/:id/notes
    PatientNotesAdd: /patients/:id/notes/add
    PatientNotesEdit:  /patients/:id/notes/edit/:noteID
    RevisionHistory: /patients/:id/revision_history
    Admin: /admin
    careadmin: /careadmin
    EditPatient: /EditPatient/:id
    EditCareprovider: /EditCarepeovider

## Files tree

    .gitignore
    README.md
    package-lock.json
    package.json
    public
        |-- favicon.ico
        |-- index.html
        |-- logo192.png
        |-- logo512.png
        |-- manifest.json
        |-- robots.txt
    src
        |-- App.css
        |-- App.js
        |-- App.test.js
        |-- components
        |   |-- images
        |   |   |-- admin-icon.png
        |   |-- pages
        |   |   |-- EditCareprovider.js
        |   |   |-- EditPatient.js
        |   |   |-- admin.js
        |   |   |-- allergies.js
        |   |   |-- allergiesAdd.js
        |   |   |-- billing.js
        |   |   |-- billingAdd.js
        |   |   |-- billingEdit.js
        |   |   |-- careadmin.js
        |   |   |-- immune.js
        |   |   |-- immuneAdd.js
        |   |   |-- lab.js
        |   |   |-- login.js
        |   |   |-- loginauth.js
        |   |   |-- medHistory.js
        |   |   |-- medHistoryAdd.js
        |   |   |-- medHistoryEdit.js
        |   |   |-- meds.js
        |   |   |-- medsAdd.js
        |   |   |-- notes.js
        |   |   |-- patient.js
        |   |   |-- patientContactDetailsEdit.js
        |   |   |-- patientContactDetailsReadOnly.js
        |   |   |-- patientDemographicsEdit.js
        |   |   |-- patientDemographicsReadOnly.js
        |   |   |-- patientEmergencyEdit.js
        |   |   |-- patientEmergencyReadOnly.js
        |   |   |-- patientLab.js
        |   |   |-- patientLabAdd.js
        |   |   |-- patientLabEdit.js
        |   |   |-- patientLabReadOnly.js
        |   |   |-- patientNotes.js
        |   |   |-- patientNotesAdd.js
        |   |   |-- patientNotesEdit.js
        |   |   |-- patientNotesReadOnly.js
        |   |   |-- patientView.js
        |   |   |-- patients.js
        |   |   |-- radio.js
        |   |   |-- radioAdd.js
        |   |   |-- radioEdit.js
        |   |   |-- revisionHistory.js
        |   |-- shared
        |   |   |-- footer.css
        |   |   |-- footer.js
        |   |   |-- header.js
        |   |   |-- navigation.css
        |   |   |-- navigation.js
        |   |-- styles
        |   |   |-- admin.css
        |   |   |-- allergies.css
        |   |   |-- billing.css
        |   |   |-- immune.css
        |   |   |-- lab.css
        |   |   |-- medHistory.css
        |   |   |-- notes.css
        |   |   |-- patient.css
        |   |   |-- patientContactDetailsReadOnly.css
        |   |   |-- patientDemographicsEdit.css
        |   |   |-- patientDemographicsReadOnly.css
        |   |   |-- patientEditContactDetails.css
        |   |   |-- patientEmergencyEdit.css
        |   |   |-- patientEmergencyReadOnly.css
        |   |   |-- patientLab.css
        |   |   |-- patientLabEdit.css
        |   |   |-- patientNotes.css
        |   |   |-- patientView.css
        |   |   |-- patientsList.css
        |   |   |-- radio.css
        |-- index.css
        |-- index.js
        |-- logo.svg
        |-- reportWebVitals.js
        |-- setupTests.js
    yarn.lock
